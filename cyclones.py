# state file generated using paraview version 5.10.1-753-gd5c898ba1f
import paraview
paraview.compatibility.major = 5
paraview.compatibility.minor = 10

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

# Create a new 'Render View'
renderView1 = CreateView('RenderView')
renderView1.ViewSize = [3222, 1978]
renderView1.AxesGrid = 'GridAxes3DActor'
renderView1.CenterOfRotation = [210.8329200744629, 74.90901947021484, -51.23817611685708]
renderView1.StereoType = 'Crystal Eyes'
renderView1.CameraPosition = [416.6657650479329, -368.4515597112297, -27.89447978936064]
renderView1.CameraFocalPoint = [279.694285596445, -63.11513224381811, -52.2693496580241]
renderView1.CameraViewUp = [-0.10181650617056422, 0.033695054637721204, 0.994232388510946]
renderView1.CameraViewAngle = 18.624
renderView1.CameraFocalDisk = 1.0
renderView1.CameraParallelScale = 126.65766643154791

SetActiveView(None)

# ----------------------------------------------------------------
# setup view layouts
# ----------------------------------------------------------------

# create new layout object 'Layout #1'
layout1 = CreateLayout(name='Layout #1')
layout1.AssignView(0, renderView1)
layout1.SetSize(3222, 1978)

# ----------------------------------------------------------------
# restore active view
SetActiveView(renderView1)
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------

# create a new 'CDIReader'
ngc2012_atm_ml_23h_inst_1_npac_20200802T010000Znc = CDIReader(registrationName='ngc2012_atm_ml_23h_inst_1_n-pac_20200802T010000Z.nc', FileNames=['/tmp/ngc2012_atm_ml_23h_inst_1_n-pac_20200802T010000Z.nc'])
ngc2012_atm_ml_23h_inst_1_npac_20200802T010000Znc.Dimensions = '(clon, clat, height)'
ngc2012_atm_ml_23h_inst_1_npac_20200802T010000Znc.CellArrayStatus = ['ua', 'va', 'ta', 'pfull']
ngc2012_atm_ml_23h_inst_1_npac_20200802T010000Znc.Show3DSurface = 1
ngc2012_atm_ml_23h_inst_1_npac_20200802T010000Znc.VerticalLevel = 44
ngc2012_atm_ml_23h_inst_1_npac_20200802T010000Znc.LayerThickness = 50
ngc2012_atm_ml_23h_inst_1_npac_20200802T010000Znc.MaskingValueVar = 'ua'

# create a new 'Clip'
clip1 = Clip(registrationName='Clip1', Input=ngc2012_atm_ml_23h_inst_1_npac_20200802T010000Znc)
clip1.ClipType = 'Plane'
clip1.HyperTreeGridClipper = 'Plane'
clip1.Scalars = ['CELLS', 'pfull']
clip1.Value = 51103.16736519337

# init the 'Plane' selected for 'ClipType'
clip1.ClipType.Origin = [0.0, 0.0, -0.25]
clip1.ClipType.Normal = [0.0, 0.0, 1.0]

# init the 'Plane' selected for 'HyperTreeGridClipper'
clip1.HyperTreeGridClipper.Origin = [-0.0003509521484375, 75.0, -0.44999998807907104]

# create a new 'Calculator'
calculator1 = Calculator(registrationName='Calculator1', Input=clip1)
calculator1.AttributeType = 'Cell Data'
calculator1.ResultArrayName = 'velmag'
calculator1.Function = 'sqrt(ua*ua+va*va)'

# create a new 'Cell Data to Point Data'
cellDatatoPointData1 = CellDatatoPointData(registrationName='CellDatatoPointData1', Input=calculator1)
cellDatatoPointData1.ProcessAllArrays = 0
cellDatatoPointData1.CellDataArraytoprocess = ['pfull', 'velmag']
cellDatatoPointData1.PassCellData = 1

# create a new 'Calculator'
calculator2 = Calculator(registrationName='Calculator2', Input=cellDatatoPointData1)
calculator2.CoordinateResults = 1
calculator2.Function = 'coordsX*iHat+coordsY*jHat-pfull*kHat*1e-3'

# create a new 'Threshold'
threshold1 = Threshold(registrationName='Threshold1', Input=calculator2)
threshold1.Scalars = ['CELLS', 'velmag']
threshold1.LowerThreshold = 24.0
threshold1.UpperThreshold = 72.49142665795071
threshold1.UseContinuousCellRange = 1

# create a new 'Contour'
contour1 = Contour(registrationName='Contour1', Input=threshold1)
contour1.ContourBy = ['POINTS', 'velmag']
contour1.Isosurfaces = [25.0]
contour1.PointMergeMethod = 'Uniform Binning'

# create a new 'Slice'
slice2 = Slice(registrationName='Slice2', Input=calculator2)
slice2.SliceType = 'Plane'
slice2.HyperTreeGridSlicer = 'Plane'
slice2.SliceOffsetValues = [0.0]

# init the 'Plane' selected for 'SliceType'
slice2.SliceType.Origin = [214.8620560311023, 95.11525319965598, -70.0]
slice2.SliceType.Normal = [0.0, 0.0, 1.0]

# init the 'Plane' selected for 'HyperTreeGridSlicer'
slice2.HyperTreeGridSlicer.Origin = [-0.0003509521484375, 75.0, -0.44999998807907104]

# create a new 'Calculator'
calculator3 = Calculator(registrationName='Calculator3', Input=slice2)
calculator3.AttributeType = 'Cell Data'
calculator3.ResultArrayName = 'vel'
calculator3.Function = 'ua*iHat+va*jHat'

# create a new 'Slice'
slice1 = Slice(registrationName='Slice1', Input=calculator2)
slice1.SliceType = 'Plane'
slice1.HyperTreeGridSlicer = 'Plane'
slice1.SliceOffsetValues = [0.0]

# init the 'Plane' selected for 'SliceType'
slice1.SliceType.Origin = [208.89304124425666, 74.20345761070587, -21.000060434112115]
slice1.SliceType.Normal = [-0.2572526177671085, -0.9585063492611922, 0.1228278025446669]

# init the 'Plane' selected for 'HyperTreeGridSlicer'
slice1.HyperTreeGridSlicer.Origin = [210.8329200744629, 74.90901947021484, -0.5499999895691872]

# create a new 'Python Calculator'
pythonCalculator1 = PythonCalculator(registrationName='PythonCalculator1', Input=calculator3)
pythonCalculator1.Expression = 'vorticity(vel)'
pythonCalculator1.ArrayAssociation = 'Cell Data'
pythonCalculator1.ArrayName = 'vort'

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# show data from contour1
contour1Display = Show(contour1, renderView1, 'GeometryRepresentation')

# get color transfer function/color map for 'pfull'
pfullLUT = GetColorTransferFunction('pfull')
pfullLUT.RGBPoints = [1438.3004150390625, 0.231373, 0.298039, 0.752941, 50413.57989501953, 0.865003, 0.865003, 0.865003, 99388.859375, 0.705882, 0.0156863, 0.14902]
pfullLUT.ScalarRangeInitialized = 1.0

# trace defaults for the display properties.
contour1Display.Representation = 'Surface'
contour1Display.ColorArrayName = ['POINTS', 'pfull']
contour1Display.LookupTable = pfullLUT
contour1Display.SelectTCoordArray = 'None'
contour1Display.SelectNormalArray = 'Normals'
contour1Display.SelectTangentArray = 'None'
contour1Display.OSPRayScaleArray = 'pfull'
contour1Display.OSPRayScaleFunction = 'PiecewiseFunction'
contour1Display.SelectOrientationVectors = 'None'
contour1Display.ScaleFactor = 17.833251190185546
contour1Display.SelectScaleArray = 'pfull'
contour1Display.GlyphType = 'Arrow'
contour1Display.GlyphTableIndexArray = 'pfull'
contour1Display.GaussianRadius = 0.8916625595092773
contour1Display.SetScaleArray = ['POINTS', 'pfull']
contour1Display.ScaleTransferFunction = 'PiecewiseFunction'
contour1Display.OpacityArray = ['POINTS', 'pfull']
contour1Display.OpacityTransferFunction = 'PiecewiseFunction'
contour1Display.DataAxesGrid = 'GridAxesRepresentation'
contour1Display.PolarAxes = 'PolarAxesRepresentation'
contour1Display.SelectInputVectors = ['POINTS', 'Normals']
contour1Display.WriteLog = ''
contour1Display.BumpMapInputDataArray = ['POINTS', 'pfull']
contour1Display.ExtrusionInputDataArray = ['POINTS', 'pfull']

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
contour1Display.ScaleTransferFunction.Points = [50477.9375, 0.0, 0.5, 0.0, 50485.9375, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
contour1Display.OpacityTransferFunction.Points = [50477.9375, 0.0, 0.5, 0.0, 50485.9375, 1.0, 0.5, 0.0]

# show data from pythonCalculator1
pythonCalculator1Display = Show(pythonCalculator1, renderView1, 'GeometryRepresentation')

# get color transfer function/color map for 'velmag'
velmagLUT = GetColorTransferFunction('velmag')
velmagLUT.AutomaticRescaleRangeMode = 'Never'
velmagLUT.RGBPoints = [0.0, 0.267004, 0.004874, 0.329415, 0.1961, 0.26851, 0.009605, 0.335427, 0.39214999999999994, 0.269944, 0.014625, 0.341379, 0.5882499999999999, 0.271305, 0.019942, 0.347269, 0.7842999999999999, 0.272594, 0.025563, 0.353093, 0.9804, 0.273809, 0.031497, 0.358853, 1.17645, 0.274952, 0.037752, 0.364543, 1.37255, 0.276022, 0.044167, 0.370164, 1.5686499999999999, 0.277018, 0.050344, 0.375715, 1.7647, 0.277941, 0.056324, 0.381191, 1.9608, 0.278791, 0.062145, 0.386592, 2.15685, 0.279566, 0.067836, 0.391917, 2.35295, 0.280267, 0.073417, 0.397163, 2.549, 0.280894, 0.078907, 0.402329, 2.7451, 0.281446, 0.08432, 0.407414, 2.9412000000000003, 0.281924, 0.089666, 0.412415, 3.13725, 0.282327, 0.094955, 0.417331, 3.3333500000000003, 0.282656, 0.100196, 0.42216, 3.5294, 0.28291, 0.105393, 0.426902, 3.7255000000000003, 0.283091, 0.110553, 0.431554, 3.92155, 0.283197, 0.11568, 0.436115, 4.117649999999999, 0.283229, 0.120777, 0.440584, 4.313750000000001, 0.283187, 0.125848, 0.44496, 4.5098, 0.283072, 0.130895, 0.449241, 4.7059, 0.282884, 0.13592, 0.453427, 4.90195, 0.282623, 0.140926, 0.457517, 5.09805, 0.28229, 0.145912, 0.46151, 5.2941, 0.281887, 0.150881, 0.465405, 5.4902, 0.281412, 0.155834, 0.469201, 5.68625, 0.280868, 0.160771, 0.472899, 5.88235, 0.280255, 0.165693, 0.476498, 6.07845, 0.279574, 0.170599, 0.479997, 6.2745, 0.278826, 0.17549, 0.483397, 6.4706, 0.278012, 0.180367, 0.486697, 6.666650000000001, 0.277134, 0.185228, 0.489898, 6.862749999999999, 0.276194, 0.190074, 0.493001, 7.0588, 0.275191, 0.194905, 0.496005, 7.2549, 0.274128, 0.199721, 0.498911, 7.4510000000000005, 0.273006, 0.20452, 0.501721, 7.64705, 0.271828, 0.209303, 0.504434, 7.8431500000000005, 0.270595, 0.214069, 0.507052, 8.039200000000001, 0.269308, 0.218818, 0.509577, 8.235299999999999, 0.267968, 0.223549, 0.512008, 8.43135, 0.26658, 0.228262, 0.514349, 8.62745, 0.265145, 0.232956, 0.516599, 8.82355, 0.263663, 0.237631, 0.518762, 9.0196, 0.262138, 0.242286, 0.520837, 9.2157, 0.260571, 0.246922, 0.522828, 9.411750000000001, 0.258965, 0.251537, 0.524736, 9.60785, 0.257322, 0.25613, 0.526563, 9.8039, 0.255645, 0.260703, 0.528312, 10.0, 0.253935, 0.265254, 0.529983, 10.1961, 0.252194, 0.269783, 0.531579, 10.39215, 0.250425, 0.27429, 0.533103, 10.58825, 0.248629, 0.278775, 0.534556, 10.7843, 0.246811, 0.283237, 0.535941, 10.9804, 0.244972, 0.287675, 0.53726, 11.17645, 0.243113, 0.292092, 0.538516, 11.372549999999999, 0.241237, 0.296485, 0.539709, 11.56865, 0.239346, 0.300855, 0.540844, 11.7647, 0.237441, 0.305202, 0.541921, 11.9608, 0.235526, 0.309527, 0.542944, 12.15685, 0.233603, 0.313828, 0.543914, 12.35295, 0.231674, 0.318106, 0.544834, 12.549, 0.229739, 0.322361, 0.545706, 12.7451, 0.227802, 0.326594, 0.546532, 12.9412, 0.225863, 0.330805, 0.547314, 13.13725, 0.223925, 0.334994, 0.548053, 13.33335, 0.221989, 0.339161, 0.548752, 13.529399999999999, 0.220057, 0.343307, 0.549413, 13.725499999999998, 0.21813, 0.347432, 0.550038, 13.92155, 0.21621, 0.351535, 0.550627, 14.117650000000001, 0.214298, 0.355619, 0.551184, 14.31375, 0.212395, 0.359683, 0.55171, 14.5098, 0.210503, 0.363727, 0.552206, 14.7059, 0.208623, 0.367752, 0.552675, 14.90195, 0.206756, 0.371758, 0.553117, 15.098049999999999, 0.204903, 0.375746, 0.553533, 15.2941, 0.203063, 0.379716, 0.553925, 15.490200000000002, 0.201239, 0.38367, 0.554294, 15.68625, 0.19943, 0.387607, 0.554642, 15.88235, 0.197636, 0.391528, 0.554969, 16.07845, 0.19586, 0.395433, 0.555276, 16.2745, 0.1941, 0.399323, 0.555565, 16.470599999999997, 0.192357, 0.403199, 0.555836, 16.66665, 0.190631, 0.407061, 0.556089, 16.862750000000002, 0.188923, 0.41091, 0.556326, 17.058799999999998, 0.187231, 0.414746, 0.556547, 17.2549, 0.185556, 0.41857, 0.556753, 17.451, 0.183898, 0.422383, 0.556944, 17.64705, 0.182256, 0.426184, 0.55712, 17.843149999999998, 0.180629, 0.429975, 0.557282, 18.0392, 0.179019, 0.433756, 0.55743, 18.2353, 0.177423, 0.437527, 0.557565, 18.43135, 0.175841, 0.44129, 0.557685, 18.62745, 0.174274, 0.445044, 0.557792, 18.82355, 0.172719, 0.448791, 0.557885, 19.0196, 0.171176, 0.45253, 0.557965, 19.2157, 0.169646, 0.456262, 0.55803, 19.41175, 0.168126, 0.459988, 0.558082, 19.60785, 0.166617, 0.463708, 0.558119, 19.8039, 0.165117, 0.467423, 0.558141, 20.0, 0.163625, 0.471133, 0.558148, 20.1961, 0.162142, 0.474838, 0.55814, 20.39215, 0.160665, 0.47854, 0.558115, 20.58825, 0.159194, 0.482237, 0.558073, 20.7843, 0.157729, 0.485932, 0.558013, 20.9804, 0.15627, 0.489624, 0.557936, 21.17645, 0.154815, 0.493313, 0.55784, 21.37255, 0.153364, 0.497, 0.557724, 21.56865, 0.151918, 0.500685, 0.557587, 21.7647, 0.150476, 0.504369, 0.55743, 21.9608, 0.149039, 0.508051, 0.55725, 22.15685, 0.147607, 0.511733, 0.557049, 22.35295, 0.14618, 0.515413, 0.556823, 22.549, 0.144759, 0.519093, 0.556572, 22.745099999999997, 0.143343, 0.522773, 0.556295, 22.941200000000002, 0.141935, 0.526453, 0.555991, 23.13725, 0.140536, 0.530132, 0.555659, 23.33335, 0.139147, 0.533812, 0.555298, 23.5294, 0.13777, 0.537492, 0.554906, 23.7255, 0.136408, 0.541173, 0.554483, 23.92155, 0.135066, 0.544853, 0.554029, 24.117649999999998, 0.133743, 0.548535, 0.553541, 24.31375, 0.132444, 0.552216, 0.553018, 24.509800000000002, 0.131172, 0.555899, 0.552459, 24.7059, 0.129933, 0.559582, 0.551864, 24.90195, 0.128729, 0.563265, 0.551229, 25.09805, 0.127568, 0.566949, 0.550556, 25.294100000000004, 0.126453, 0.570633, 0.549841, 25.4902, 0.125394, 0.574318, 0.549086, 25.68625, 0.124395, 0.578002, 0.548287, 25.88235, 0.123463, 0.581687, 0.547445, 26.078449999999997, 0.122606, 0.585371, 0.546557, 26.2745, 0.121831, 0.589055, 0.545623, 26.4706, 0.121148, 0.592739, 0.544641, 26.666649999999997, 0.120565, 0.596422, 0.543611, 26.862750000000002, 0.120092, 0.600104, 0.54253, 27.058799999999998, 0.119738, 0.603785, 0.5414, 27.2549, 0.119512, 0.607464, 0.540218, 27.450999999999997, 0.119423, 0.611141, 0.538982, 27.64705, 0.119483, 0.614817, 0.537692, 27.84315, 0.119699, 0.61849, 0.536347, 28.039199999999997, 0.120081, 0.622161, 0.534946, 28.235300000000002, 0.120638, 0.625828, 0.533488, 28.43135, 0.12138, 0.629492, 0.531973, 28.62745, 0.122312, 0.633153, 0.530398, 28.823549999999997, 0.123444, 0.636809, 0.528763, 29.0196, 0.12478, 0.640461, 0.527068, 29.2157, 0.126326, 0.644107, 0.525311, 29.411749999999998, 0.128087, 0.647749, 0.523491, 29.607850000000003, 0.130067, 0.651384, 0.521608, 29.8039, 0.132268, 0.655014, 0.519661, 30.0, 0.134692, 0.658636, 0.517649, 30.196099999999998, 0.137339, 0.662252, 0.515571, 30.39215, 0.14021, 0.665859, 0.513427, 30.58825, 0.143303, 0.669459, 0.511215, 30.784299999999998, 0.146616, 0.67305, 0.508936, 30.980400000000003, 0.150148, 0.676631, 0.506589, 31.17645, 0.153894, 0.680203, 0.504172, 31.37255, 0.157851, 0.683765, 0.501686, 31.568649999999998, 0.162016, 0.687316, 0.499129, 31.7647, 0.166383, 0.690856, 0.496502, 31.9608, 0.170948, 0.694384, 0.493803, 32.15685, 0.175707, 0.6979, 0.491033, 32.35295, 0.180653, 0.701402, 0.488189, 32.549, 0.185783, 0.704891, 0.485273, 32.7451, 0.19109, 0.708366, 0.482284, 32.941199999999995, 0.196571, 0.711827, 0.479221, 33.13725, 0.202219, 0.715272, 0.476084, 33.33335, 0.20803, 0.718701, 0.472873, 33.529399999999995, 0.214, 0.722114, 0.469588, 33.725500000000004, 0.220124, 0.725509, 0.466226, 33.92155, 0.226397, 0.728888, 0.462789, 34.11765, 0.232815, 0.732247, 0.459277, 34.31375, 0.239374, 0.735588, 0.455688, 34.5098, 0.24607, 0.73891, 0.452024, 34.7059, 0.252899, 0.742211, 0.448284, 34.90195, 0.259857, 0.745492, 0.444467, 35.09805, 0.266941, 0.748751, 0.440573, 35.2941, 0.274149, 0.751988, 0.436601, 35.4902, 0.281477, 0.755203, 0.432552, 35.68625, 0.288921, 0.758394, 0.428426, 35.88235, 0.296479, 0.761561, 0.424223, 36.078450000000004, 0.304148, 0.764704, 0.419943, 36.274499999999996, 0.311925, 0.767822, 0.415586, 36.4706, 0.319809, 0.770914, 0.411152, 36.666650000000004, 0.327796, 0.77398, 0.40664, 36.86275, 0.335885, 0.777018, 0.402049, 37.0588, 0.344074, 0.780029, 0.397381, 37.2549, 0.35236, 0.783011, 0.392636, 37.451, 0.360741, 0.785964, 0.387814, 37.64705, 0.369214, 0.788888, 0.382914, 37.843149999999994, 0.377779, 0.791781, 0.377939, 38.0392, 0.386433, 0.794644, 0.372886, 38.2353, 0.395174, 0.797475, 0.367757, 38.431349999999995, 0.404001, 0.800275, 0.362552, 38.62745, 0.412913, 0.803041, 0.357269, 38.823550000000004, 0.421908, 0.805774, 0.35191, 39.0196, 0.430983, 0.808473, 0.346476, 39.2157, 0.440137, 0.811138, 0.340967, 39.41175, 0.449368, 0.813768, 0.335384, 39.60785, 0.458674, 0.816363, 0.329727, 39.8039, 0.468053, 0.818921, 0.323998, 40.0, 0.477504, 0.821444, 0.318195, 40.1961, 0.487026, 0.823929, 0.312321, 40.39215, 0.496615, 0.826376, 0.306377, 40.588249999999995, 0.506271, 0.828786, 0.300362, 40.7843, 0.515992, 0.831158, 0.294279, 40.9804, 0.525776, 0.833491, 0.288127, 41.176449999999996, 0.535621, 0.835785, 0.281908, 41.372550000000004, 0.545524, 0.838039, 0.275626, 41.56865, 0.555484, 0.840254, 0.269281, 41.7647, 0.565498, 0.84243, 0.262877, 41.9608, 0.575563, 0.844566, 0.256415, 42.15685, 0.585678, 0.846661, 0.249897, 42.35295, 0.595839, 0.848717, 0.243329, 42.549, 0.606045, 0.850733, 0.236712, 42.7451, 0.616293, 0.852709, 0.230052, 42.9412, 0.626579, 0.854645, 0.223353, 43.13725, 0.636902, 0.856542, 0.21662, 43.333349999999996, 0.647257, 0.8584, 0.209861, 43.5294, 0.657642, 0.860219, 0.203082, 43.725500000000004, 0.668054, 0.861999, 0.196293, 43.921549999999996, 0.678489, 0.863742, 0.189503, 44.117650000000005, 0.688944, 0.865448, 0.182725, 44.31375, 0.699415, 0.867117, 0.175971, 44.5098, 0.709898, 0.868751, 0.169257, 44.7059, 0.720391, 0.87035, 0.162603, 44.90195, 0.730889, 0.871916, 0.156029, 45.09805, 0.741388, 0.873449, 0.149561, 45.2941, 0.751884, 0.874951, 0.143228, 45.490199999999994, 0.762373, 0.876424, 0.137064, 45.68625, 0.772852, 0.877868, 0.131109, 45.88235, 0.783315, 0.879285, 0.125405, 46.07845, 0.79376, 0.880678, 0.120005, 46.2745, 0.804182, 0.882046, 0.114965, 46.4706, 0.814576, 0.883393, 0.110347, 46.66665, 0.82494, 0.88472, 0.106217, 46.86275, 0.83527, 0.886029, 0.102646, 47.0588, 0.845561, 0.887322, 0.099702, 47.2549, 0.85581, 0.888601, 0.097452, 47.451, 0.866013, 0.889868, 0.095953, 47.64705, 0.876168, 0.891125, 0.09525, 47.84315, 0.886271, 0.892374, 0.095374, 48.0392, 0.89632, 0.893616, 0.096335, 48.235299999999995, 0.906311, 0.894855, 0.098125, 48.43135, 0.916242, 0.896091, 0.100717, 48.62745, 0.926106, 0.89733, 0.104071, 48.82355, 0.935904, 0.89857, 0.108131, 49.019600000000004, 0.945636, 0.899815, 0.112838, 49.2157, 0.9553, 0.901065, 0.118128, 49.41175, 0.964894, 0.902323, 0.123941, 49.60785, 0.974417, 0.90359, 0.130215, 49.8039, 0.983868, 0.904867, 0.136897, 50.0, 0.993248, 0.906157, 0.143936]
velmagLUT.NanColor = [1.0, 0.0, 0.0]
velmagLUT.ScalarRangeInitialized = 1.0

# trace defaults for the display properties.
pythonCalculator1Display.Representation = 'Surface'
pythonCalculator1Display.ColorArrayName = ['CELLS', 'velmag']
pythonCalculator1Display.LookupTable = velmagLUT
pythonCalculator1Display.SelectTCoordArray = 'None'
pythonCalculator1Display.SelectNormalArray = 'None'
pythonCalculator1Display.SelectTangentArray = 'None'
pythonCalculator1Display.OSPRayScaleArray = 'pfull'
pythonCalculator1Display.OSPRayScaleFunction = 'PiecewiseFunction'
pythonCalculator1Display.SelectOrientationVectors = 'vel'
pythonCalculator1Display.ScaleFactor = 17.833251190185546
pythonCalculator1Display.SelectScaleArray = 'velmag'
pythonCalculator1Display.GlyphType = 'Arrow'
pythonCalculator1Display.GlyphTableIndexArray = 'velmag'
pythonCalculator1Display.GaussianRadius = 0.8916625595092773
pythonCalculator1Display.SetScaleArray = ['POINTS', 'pfull']
pythonCalculator1Display.ScaleTransferFunction = 'PiecewiseFunction'
pythonCalculator1Display.OpacityArray = ['POINTS', 'pfull']
pythonCalculator1Display.OpacityTransferFunction = 'PiecewiseFunction'
pythonCalculator1Display.DataAxesGrid = 'GridAxesRepresentation'
pythonCalculator1Display.PolarAxes = 'PolarAxesRepresentation'
pythonCalculator1Display.SelectInputVectors = ['CELLS', 'vel']
pythonCalculator1Display.WriteLog = ''
pythonCalculator1Display.BumpMapInputDataArray = ['POINTS', 'pfull']
pythonCalculator1Display.ExtrusionInputDataArray = ['CELLS', 'velmag']

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
pythonCalculator1Display.ScaleTransferFunction.Points = [70000.0, 0.0, 0.5, 0.0, 70016.0, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
pythonCalculator1Display.OpacityTransferFunction.Points = [70000.0, 0.0, 0.5, 0.0, 70016.0, 1.0, 0.5, 0.0]

# setup the color legend parameters for each legend in this view

# get color legend/bar for velmagLUT in view renderView1
velmagLUTColorBar = GetScalarBar(velmagLUT, renderView1)
velmagLUTColorBar.WindowLocation = 'Upper Right Corner'
velmagLUTColorBar.Title = 'velmag'
velmagLUTColorBar.ComponentTitle = ''

# set color bar visibility
velmagLUTColorBar.Visibility = 1

# get color legend/bar for pfullLUT in view renderView1
pfullLUTColorBar = GetScalarBar(pfullLUT, renderView1)
pfullLUTColorBar.Title = 'pfull'
pfullLUTColorBar.ComponentTitle = ''

# set color bar visibility
pfullLUTColorBar.Visibility = 1

# show color legend
contour1Display.SetScalarBarVisibility(renderView1, True)

# show color legend
pythonCalculator1Display.SetScalarBarVisibility(renderView1, True)

# ----------------------------------------------------------------
# setup color maps and opacity mapes used in the visualization
# note: the Get..() functions create a new object, if needed
# ----------------------------------------------------------------

# get opacity transfer function/opacity map for 'velmag'
velmagPWF = GetOpacityTransferFunction('velmag')
velmagPWF.Points = [0.0, 0.0, 0.5, 0.0, 50.0, 1.0, 0.5, 0.0]
velmagPWF.ScalarRangeInitialized = 1

# get opacity transfer function/opacity map for 'pfull'
pfullPWF = GetOpacityTransferFunction('pfull')
pfullPWF.Points = [1438.3004150390625, 0.0, 0.5, 0.0, 99388.859375, 1.0, 0.5, 0.0]
pfullPWF.ScalarRangeInitialized = 1

# ----------------------------------------------------------------
# restore active source
SetActiveSource(pythonCalculator1)
# ----------------------------------------------------------------


if __name__ == '__main__':
    # generate extracts
    SaveExtracts(ExtractsOutputDirectory='extracts')
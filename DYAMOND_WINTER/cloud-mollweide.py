# state file generated using paraview version 5.9.0-RC4-343-gd9b21ea103

#### import the simple module from the paraview
from paraview.simple import *
import run_pv_ao
import os

#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()


options = run_pv_ao.parse_args()

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

# Create a new 'Render View'
renderView1 = CreateView('RenderView')
renderView1.ViewSize = [1920, 1080]
renderView1.InteractionMode = '2D'
renderView1.AxesGrid = 'GridAxes3DActor'
renderView1.OrientationAxesVisibility = 0
renderView1.CenterOfRotation = [-3.0517578125e-05, 0.0, 0.0]
renderView1.UseLight = 0
renderView1.StereoType = 'Crystal Eyes'
renderView1.CameraPosition = [26.167212008366825, 0.7676929849244682, 10000.0]
renderView1.CameraFocalPoint = [26.167212008366825, 0.7676929849244682, 0.0]
renderView1.CameraFocalDisk = 1.0
renderView1.CameraParallelScale = 207.27710592960645
renderView1.Background = [0.12983901731898986, 0.12983901731898986, 0.12983901731898986]

SetActiveView(None)

# ----------------------------------------------------------------
# setup view layouts
# ----------------------------------------------------------------

# create new layout object 'Layout #1'
layout1 = CreateLayout(name='Layout #1')
layout1.AssignView(0, renderView1)
layout1.SetSize(1920, 1080)

# ----------------------------------------------------------------
# restore active view
SetActiveView(renderView1)
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------
print (options)
# create a new 'CDIReader'
ocean = CDIReader(registrationName=options["oce"][0], FileNames=options["oce"])
ocean.Dimensions = '(clon, clat, depth)'
ocean.PointArrayStatus = []
ocean.CellArrayStatus = ['u', 'v']
ocean.SetProjection = 'Mollweide Projection'
ocean.LayerThickness = 50
ocean.MaskingValueVar = 'to'

# create a new 'Calculator'
calc_vel = Calculator(registrationName='Calc_Vel', Input=ocean)
calc_vel.AttributeType = 'Cell Data'
calc_vel.ResultArrayName = 'velmag'
calc_vel.Function = 'sqrt(u*u+v*v)'

# create a new 'CDIReader'
clouds = CDIReader(registrationName='clouds', FileNames=options["atm"])
clouds.Dimensions = '(clon, clat, sfc)'
clouds.CellArrayStatus = ['cllvi', 'clivi']
clouds.SetProjection = 'Mollweide Projection'
clouds.LayerThickness = 50
clouds.MaskingValueVar = 'cllvi'

# create a new 'Texture Map to Plane'
textureMaptoPlane1 = TextureMaptoPlane(registrationName='TextureMaptoPlane1', Input=clouds)

# create a new 'Calculator'
calc_alpha = Calculator(registrationName='Calc_Alpha', Input=clouds)
calc_alpha.AttributeType = 'Cell Data'
calc_alpha.ResultArrayName = 'alpha'
calc_alpha.Function = '(3/2 * (cllvi +clivi*5)*100)/ (3/2 * (cllvi +clivi*5)*100+7)'

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# show data from calc_alpha
calc_alphaDisplay = Show(calc_alpha, renderView1, 'UnstructuredGridRepresentation')

# get color transfer function/color map for 'alpha'
alphaLUT = GetColorTransferFunction('alpha')
alphaLUT.AutomaticRescaleRangeMode = 'Never'
alphaLUT.EnableOpacityMapping = 1
alphaLUT.RGBPoints = [0.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0]
alphaLUT.ColorSpace = 'RGB'
alphaLUT.ScalarRangeInitialized = 1.0

# get opacity transfer function/opacity map for 'alpha'
alphaPWF = GetOpacityTransferFunction('alpha')
alphaPWF.ScalarRangeInitialized = 1

# trace defaults for the display properties.
calc_alphaDisplay.Representation = 'Surface'
calc_alphaDisplay.ColorArrayName = ['CELLS', 'alpha']
calc_alphaDisplay.LookupTable = alphaLUT
calc_alphaDisplay.SelectTCoordArray = 'None'
calc_alphaDisplay.SelectNormalArray = 'None'
calc_alphaDisplay.SelectTangentArray = 'None'
calc_alphaDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
calc_alphaDisplay.SelectOrientationVectors = 'None'
calc_alphaDisplay.ScaleFactor = 60.166662597656256
calc_alphaDisplay.SelectScaleArray = 'alpha'
calc_alphaDisplay.GlyphType = 'Arrow'
calc_alphaDisplay.GlyphTableIndexArray = 'alpha'
calc_alphaDisplay.GaussianRadius = 3.0083331298828124
calc_alphaDisplay.SetScaleArray = [None, '']
calc_alphaDisplay.ScaleTransferFunction = 'PiecewiseFunction'
calc_alphaDisplay.OpacityArray = [None, '']
calc_alphaDisplay.OpacityTransferFunction = 'PiecewiseFunction'
calc_alphaDisplay.DataAxesGrid = 'GridAxesRepresentation'
calc_alphaDisplay.PolarAxes = 'PolarAxesRepresentation'
calc_alphaDisplay.ScalarOpacityFunction = alphaPWF
calc_alphaDisplay.ScalarOpacityUnitDistance = 2.437963646491426
calc_alphaDisplay.OpacityArrayName = ['CELLS', 'alpha']
calc_alphaDisplay.BumpMapInputDataArray = [None, '']
calc_alphaDisplay.ExtrusionInputDataArray = ['CELLS', 'alpha']
calc_alphaDisplay.SelectInputVectors = [None, '']
calc_alphaDisplay.WriteLog = ''

# show data from calc_vel
calc_velDisplay = Show(calc_vel, renderView1, 'UnstructuredGridRepresentation')

# get color transfer function/color map for 'velmag'
velmagLUT = GetColorTransferFunction('velmag')
velmagLUT.AutomaticRescaleRangeMode = 'Never'
velmagLUT.RGBPoints = [0.0, 0.031373, 0.25098, 0.505882, 0.062745, 0.031373, 0.329719, 0.590527, 0.12549, 0.031911, 0.408397, 0.674787, 0.1882355, 0.100807, 0.479262, 0.710219, 0.2509805, 0.169704, 0.550219, 0.745744, 0.3137255, 0.238601, 0.62699, 0.787082, 0.3764705, 0.307958, 0.703114, 0.826759, 0.4392155, 0.39654, 0.752326, 0.797232, 0.501960785, 0.485121, 0.801046, 0.767705, 0.564706, 0.573702, 0.83451, 0.738178, 0.627451, 0.661592, 0.867743, 0.711034, 0.690196, 0.732457, 0.895302, 0.74253, 0.7529410000000001, 0.801845, 0.922307, 0.774579, 0.8156865, 0.841215, 0.938055, 0.817885, 0.8784315, 0.880907, 0.95391, 0.861084, 0.9411765000000001, 0.926182, 0.971626, 0.902422, 1.0, 0.968627, 0.988235, 0.941176]
velmagLUT.ColorSpace = 'Lab'
velmagLUT.ScalarRangeInitialized = 1.0

# get opacity transfer function/opacity map for 'velmag'
velmagPWF = GetOpacityTransferFunction('velmag')
velmagPWF.ScalarRangeInitialized = 1

# trace defaults for the display properties.
calc_velDisplay.Representation = 'Surface'
calc_velDisplay.ColorArrayName = ['CELLS', 'velmag']
calc_velDisplay.LookupTable = velmagLUT
calc_velDisplay.SelectTCoordArray = 'None'
calc_velDisplay.SelectNormalArray = 'None'
calc_velDisplay.SelectTangentArray = 'None'
calc_velDisplay.OSPRayScaleArray = 'vort'
calc_velDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
calc_velDisplay.SelectOrientationVectors = 'None'
calc_velDisplay.ScaleFactor = 59.99998779296875
calc_velDisplay.SelectScaleArray = 'None'
calc_velDisplay.GlyphType = 'Arrow'
calc_velDisplay.GlyphTableIndexArray = 'None'
calc_velDisplay.GaussianRadius = 2.9999993896484374
calc_velDisplay.SetScaleArray = ['POINTS', 'vort']
calc_velDisplay.ScaleTransferFunction = 'PiecewiseFunction'
calc_velDisplay.OpacityArray = ['POINTS', 'vort']
calc_velDisplay.OpacityTransferFunction = 'PiecewiseFunction'
calc_velDisplay.DataAxesGrid = 'GridAxesRepresentation'
calc_velDisplay.PolarAxes = 'PolarAxesRepresentation'
calc_velDisplay.ScalarOpacityFunction = velmagPWF
calc_velDisplay.ScalarOpacityUnitDistance = 2.693242001422205
calc_velDisplay.OpacityArrayName = ['POINTS', 'vort']
calc_velDisplay.BumpMapInputDataArray = ['POINTS', 'vort']
calc_velDisplay.ExtrusionInputDataArray = ['POINTS', 'vort']
calc_velDisplay.SelectInputVectors = [None, '']
calc_velDisplay.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
calc_velDisplay.ScaleTransferFunction.Points = [-0.0004456962051335722, 0.0, 0.5, 0.0, 0.0004504297976382077, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
calc_velDisplay.OpacityTransferFunction.Points = [-0.0004456962051335722, 0.0, 0.5, 0.0, 0.0004504297976382077, 1.0, 0.5, 0.0]

# show data from textureMaptoPlane1
textureMaptoPlane1Display = Show(textureMaptoPlane1, renderView1, 'UnstructuredGridRepresentation')

# a texture
world = CreateTexture(os.path.expanduser('~') + '/Paraview/world.topo.200402.3x5k_moll.jpg')

# trace defaults for the display properties.
textureMaptoPlane1Display.Representation = 'Surface'
textureMaptoPlane1Display.ColorArrayName = ['POINTS', '']
textureMaptoPlane1Display.SelectTCoordArray = 'Texture Coordinates'
textureMaptoPlane1Display.SelectNormalArray = 'None'
textureMaptoPlane1Display.SelectTangentArray = 'None'
textureMaptoPlane1Display.Texture = world
textureMaptoPlane1Display.Position = [0.0, 0.0, -1.0]
textureMaptoPlane1Display.OSPRayScaleArray = 'Texture Coordinates'
textureMaptoPlane1Display.OSPRayScaleFunction = 'PiecewiseFunction'
textureMaptoPlane1Display.SelectOrientationVectors = 'None'
textureMaptoPlane1Display.ScaleFactor = 67.87527770996094
textureMaptoPlane1Display.SelectScaleArray = 'None'
textureMaptoPlane1Display.GlyphType = 'Arrow'
textureMaptoPlane1Display.GlyphTableIndexArray = 'None'
textureMaptoPlane1Display.GaussianRadius = 3.3937638854980468
textureMaptoPlane1Display.SetScaleArray = ['POINTS', 'Texture Coordinates']
textureMaptoPlane1Display.ScaleTransferFunction = 'PiecewiseFunction'
textureMaptoPlane1Display.OpacityArray = ['POINTS', 'Texture Coordinates']
textureMaptoPlane1Display.OpacityTransferFunction = 'PiecewiseFunction'
textureMaptoPlane1Display.DataAxesGrid = 'GridAxesRepresentation'
textureMaptoPlane1Display.PolarAxes = 'PolarAxesRepresentation'
textureMaptoPlane1Display.ScalarOpacityUnitDistance = 2.7518974568691785
textureMaptoPlane1Display.OpacityArrayName = ['POINTS', 'Texture Coordinates']
textureMaptoPlane1Display.BumpMapInputDataArray = [None, '']
textureMaptoPlane1Display.ExtrusionInputDataArray = ['POINTS', 'Texture Coordinates']
textureMaptoPlane1Display.SelectInputVectors = [None, '']
textureMaptoPlane1Display.WriteLog = ''

# init the 'PolarAxesRepresentation' selected for 'PolarAxes'
textureMaptoPlane1Display.PolarAxes.Translation = [0.0, 0.0, -1.0]

# setup the color legend parameters for each legend in this view

# get color legend/bar for velmagLUT in view renderView1
velmagLUTColorBar = GetScalarBar(velmagLUT, renderView1)
velmagLUTColorBar.WindowLocation = 'AnyLocation'
velmagLUTColorBar.Position = [0.9313151041666666, 0.3509259259259259]
velmagLUTColorBar.Title = 'Surface current speed (m/s)'
velmagLUTColorBar.ComponentTitle = ''
velmagLUTColorBar.TitleFontSize = 32
velmagLUTColorBar.LabelFontSize = 32
velmagLUTColorBar.RangeLabelFormat = '%.1f'

# set color bar visibility
velmagLUTColorBar.Visibility = 1

# show color legend
calc_velDisplay.SetScalarBarVisibility(renderView1, True)

# ----------------------------------------------------------------
# setup color maps and opacity mapes used in the visualization
# note: the Get..() functions create a new object, if needed
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# restore active source
SetActiveSource(ocean)
# ----------------------------------------------------------------

reader=GetActiveSource()
tsteps=reader.TimestepValues
view = GetActiveView()
bna = os.path.basename(options["atm"][0])[:-3]
bno = os.path.basename(options["oce"][0])[:-3]
for n,t in enumerate (tsteps):
    print ("rendering for time %f"%t)
    view.ViewTime = t
    SaveScreenshot("clouds-ocean-%s-%s_%04d.png"%(bna,bno , n))

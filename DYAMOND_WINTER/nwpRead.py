# state file generated using paraview version 5.7.0-RC3-442-g1b4ad14

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

# trace generated using paraview version 5.7.0-RC3-442-g1b4ad14
#
# To ensure correct image size when batch processing, please search 
# for and uncomment the line `# renderView*.ViewSize = [*,*]`

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# get the material library
materialLibrary1 = GetMaterialLibrary()

# Create a new 'Render View'
renderView1 = CreateView('RenderView')
renderView1.ViewSize = [1849, 1502]
renderView1.InteractionMode = '2D'
renderView1.AxesGrid = 'GridAxes3DActor'
renderView1.StereoType = 'Crystal Eyes'
renderView1.CameraPosition = [0.0, 0.0, 10000.0]
renderView1.CameraFocalDisk = 1.0
renderView1.CameraParallelScale = 335.3697267806529
renderView1.Background = [0.32, 0.34, 0.43]
renderView1.BackEnd = 'OSPRay raycaster'
renderView1.OSPRayMaterialLibrary = materialLibrary1

SetActiveView(None)

# ----------------------------------------------------------------
# setup view layouts
# ----------------------------------------------------------------

# create new layout object 'Layout #1'
layout1 = CreateLayout(name='Layout #1')
layout1.AssignView(0, renderView1)

# ----------------------------------------------------------------
# restore active view
SetActiveView(renderView1)
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------

# create a new 'CDIReader'
nwp0005_atm_2d_ml_20200218T000000Znc = CDIReader(FileNames=['/home/dkrz/k202134/Paraview/DYAMOND_WINTER/nwp0005/atm/babylon/nwp0005_atm_2d_ml_20200218T000000Z.nc'])
nwp0005_atm_2d_ml_20200218T000000Znc.Dimensions = '(lon, lat, sfc)'
nwp0005_atm_2d_ml_20200218T000000Znc.CellArrayStatus = ['t_g', 'tqc_dia', 'tqi_dia']
nwp0005_atm_2d_ml_20200218T000000Znc.LayerThickness = 50

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# show data from nwp0005_atm_2d_ml_20200218T000000Znc
nwp0005_atm_2d_ml_20200218T000000ZncDisplay = Show(nwp0005_atm_2d_ml_20200218T000000Znc, renderView1)

# get color transfer function/color map for 't_g'
t_gLUT = GetColorTransferFunction('t_g')
t_gLUT.RGBPoints = [215.9298858642578, 0.231373, 0.298039, 0.752941, 267.4197769165039, 0.865003, 0.865003, 0.865003, 318.90966796875, 0.705882, 0.0156863, 0.14902]
t_gLUT.ScalarRangeInitialized = 1.0

# get opacity transfer function/opacity map for 't_g'
t_gPWF = GetOpacityTransferFunction('t_g')
t_gPWF.Points = [215.9298858642578, 0.0, 0.5, 0.0, 318.90966796875, 1.0, 0.5, 0.0]
t_gPWF.ScalarRangeInitialized = 1

# trace defaults for the display properties.
nwp0005_atm_2d_ml_20200218T000000ZncDisplay.Representation = 'Surface'
nwp0005_atm_2d_ml_20200218T000000ZncDisplay.ColorArrayName = ['CELLS', 't_g']
nwp0005_atm_2d_ml_20200218T000000ZncDisplay.LookupTable = t_gLUT
nwp0005_atm_2d_ml_20200218T000000ZncDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
nwp0005_atm_2d_ml_20200218T000000ZncDisplay.SelectOrientationVectors = 'None'
nwp0005_atm_2d_ml_20200218T000000ZncDisplay.ScaleFactor = 59.999993896484376
nwp0005_atm_2d_ml_20200218T000000ZncDisplay.SelectScaleArray = 'None'
nwp0005_atm_2d_ml_20200218T000000ZncDisplay.GlyphType = 'Arrow'
nwp0005_atm_2d_ml_20200218T000000ZncDisplay.GlyphTableIndexArray = 'None'
nwp0005_atm_2d_ml_20200218T000000ZncDisplay.GaussianRadius = 2.999999694824219
nwp0005_atm_2d_ml_20200218T000000ZncDisplay.SetScaleArray = [None, '']
nwp0005_atm_2d_ml_20200218T000000ZncDisplay.ScaleTransferFunction = 'PiecewiseFunction'
nwp0005_atm_2d_ml_20200218T000000ZncDisplay.OpacityArray = [None, '']
nwp0005_atm_2d_ml_20200218T000000ZncDisplay.OpacityTransferFunction = 'PiecewiseFunction'
nwp0005_atm_2d_ml_20200218T000000ZncDisplay.DataAxesGrid = 'GridAxesRepresentation'
nwp0005_atm_2d_ml_20200218T000000ZncDisplay.PolarAxes = 'PolarAxesRepresentation'
nwp0005_atm_2d_ml_20200218T000000ZncDisplay.ScalarOpacityFunction = t_gPWF
nwp0005_atm_2d_ml_20200218T000000ZncDisplay.ScalarOpacityUnitDistance = 2.432556400997915
nwp0005_atm_2d_ml_20200218T000000ZncDisplay.BumpMapInputDataArray = [None, '']
nwp0005_atm_2d_ml_20200218T000000ZncDisplay.ExtrusionInputDataArray = ['CELLS', 'cape_ml']
nwp0005_atm_2d_ml_20200218T000000ZncDisplay.InputVectors = [None, '']
nwp0005_atm_2d_ml_20200218T000000ZncDisplay.SelectInputVectors = [None, '']
nwp0005_atm_2d_ml_20200218T000000ZncDisplay.WriteLog = ''

# setup the color legend parameters for each legend in this view

# get color legend/bar for t_gLUT in view renderView1
t_gLUTColorBar = GetScalarBar(t_gLUT, renderView1)
t_gLUTColorBar.Title = 't_g'
t_gLUTColorBar.ComponentTitle = ''

# set color bar visibility
t_gLUTColorBar.Visibility = 1

# show color legend
nwp0005_atm_2d_ml_20200218T000000ZncDisplay.SetScalarBarVisibility(renderView1, True)

# ----------------------------------------------------------------
# setup color maps and opacity mapes used in the visualization
# note: the Get..() functions create a new object, if needed
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# finally, restore active source
SetActiveSource(nwp0005_atm_2d_ml_20200218T000000Znc)
# ----------------------------------------------------------------
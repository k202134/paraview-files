#!/usr/bin/env pvbatch

import sys
sys.path.append ("/usr/local/Caskroom/miniconda/base/lib/python3.7/site-packages/")


from paraview.simple import * 



ta_r2b6nc = CDIReader(registrationName='ta_r2b6.nc', FileNames=['/Users/flo/NO_BACKUP/DYAMOND_WINTER/dpp0014/R02B06/ta_r2b6.nc'])
ta_r2b6nc.Dimensions = '(clon, clat, height)'
ta_r2b6nc.CellArrayStatus = ['ta']
ta_r2b6nc.VerticalLevel = 38
ta_r2b6nc.LayerThickness = 50
ta_r2b6nc.MaskingValueVar = 'ta'
ta_r2b6nc.SetProjection = 'Catalyst (no scaling)'
ta_r2b6nc.Show3DSurface = 1

extractLocation2 = ExtractLocation(registrationName='ExtractLocation2', Input=ta_r2b6nc)
extractLocation2.Location = [0.008831620216369629, 0.0, 0.0]


SaveData('/tmp/points2.csv', proxy=extractLocation2, CellDataArrays=['ta'],
    FieldDataArrays=['time_calendar', 'time_units'],
    FieldAssociation='Cell Data',
    AddTime=1)

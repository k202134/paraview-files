# state file generated using paraview version 5.8.1

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

# trace generated using paraview version 5.8.1
#
# To ensure correct image size when batch processing, please search 
# for and uncomment the line `# renderView*.ViewSize = [*,*]`

#### import the simple module from the paraview
from paraview.simple import *
import run_pv_ao
import os

options=run_pv_ao.parse_args()
my_id="zos_eff_"+os.path.basename(options["oce"][0])[:-3].split("_")[-1]
print (my_id)

#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# Create a new 'Render View'
renderView1 = CreateView('RenderView')
renderView1.ViewSize = [2176, 1501]
renderView1.InteractionMode = '2D'
renderView1.AxesGrid = 'GridAxes3DActor'
renderView1.OrientationAxesVisibility = 0
renderView1.CenterOfRotation = [-3.0517578125e-05, 0.0, 0.0]
renderView1.UseLight = 0
renderView1.StereoType = 'Crystal Eyes'
renderView1.CameraPosition = [8.203963620786011, -158.10905243156412, 10000.0]
renderView1.CameraFocalPoint = [8.203963620786011, -158.10905243156412, 0.0]
renderView1.CameraFocalDisk = 1.0
renderView1.CameraParallelScale = 42.36394000569116

SetActiveView(None)

# ----------------------------------------------------------------
# setup view layouts
# ----------------------------------------------------------------

# create new layout object 'Layout #1'
layout1 = CreateLayout(name='Layout #1')
layout1.AssignView(0, renderView1)

# ----------------------------------------------------------------
# restore active view
SetActiveView(renderView1)
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------

# create a new 'CDIReader'
oce_file = CDIReader(FileNames=options["oce"])
oce_file.Dimensions = '(lon, lat, sfc)'
oce_file.CellArrayStatus = ['hi', 'hs', 'zos', 'conc']
oce_file.SetProjection = 'Cassini Projection'
oce_file.LayerThickness = 50

# create a new 'Calculator'
zos_eff = Calculator(Input=oce_file)
zos_eff.AttributeType = 'Cell Data'
zos_eff.ResultArrayName = 'zos_eff'
zos_eff.Function = 'zos-.9*hi*conc-.3*hs*conc'

# create a new 'CDIReader'
atm_file = CDIReader(FileNames=options["atm"])
atm_file.Dimensions = '(lon, lat, sfc)'
atm_file.CellArrayStatus = ['psl']
atm_file.SetProjection = 'Cassini Projection'
atm_file.LayerThickness = 50

# create a new 'Cell Data to Point Data'
psl_as_point = CellDatatoPointData(Input=atm_file)
psl_as_point.ProcessAllArrays = 0
psl_as_point.CellDataArraytoprocess = ['psl']

# create a new 'Contour'
psl_contour = Contour(Input=psl_as_point)
psl_contour.ContourBy = ['POINTS', 'psl']
psl_contour.Isosurfaces = [97000.0, 96000.0, 95000.0, 99000.0, 98000.0]
psl_contour.PointMergeMethod = 'Uniform Binning'

# create a new 'Threshold'
threshold1 = Threshold(Input=zos_eff)
threshold1.Scalars = ['CELLS', 'Result']
threshold1.ThresholdRange = [-5.0, -0.0]

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# show data from atm_file
atm_fileDisplay = Show(atm_file, renderView1, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
atm_fileDisplay.Representation = 'Surface'
atm_fileDisplay.AmbientColor = [0.0, 0.0, 0.0]
atm_fileDisplay.ColorArrayName = ['POINTS', '']
atm_fileDisplay.DiffuseColor = [0.0, 0.0, 0.0]
atm_fileDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
atm_fileDisplay.SelectOrientationVectors = 'None'
atm_fileDisplay.ScaleFactor = 59.99998779296875
atm_fileDisplay.SelectScaleArray = 'None'
atm_fileDisplay.GlyphType = 'Arrow'
atm_fileDisplay.GlyphTableIndexArray = 'None'
atm_fileDisplay.GaussianRadius = 2.9999993896484374
atm_fileDisplay.SetScaleArray = [None, '']
atm_fileDisplay.ScaleTransferFunction = 'PiecewiseFunction'
atm_fileDisplay.OpacityArray = [None, '']
atm_fileDisplay.OpacityTransferFunction = 'PiecewiseFunction'
atm_fileDisplay.DataAxesGrid = 'GridAxesRepresentation'
atm_fileDisplay.PolarAxes = 'PolarAxesRepresentation'
atm_fileDisplay.ScalarOpacityUnitDistance = 2.432556203035958
atm_fileDisplay.BumpMapInputDataArray = [None, '']
atm_fileDisplay.ExtrusionInputDataArray = ['CELLS', 'Center Latitude (CLAT)']
atm_fileDisplay.SelectInputVectors = [None, '']
atm_fileDisplay.WriteLog = ''

# show data from zos_eff
zos_effDisplay = Show(zos_eff, renderView1, 'UnstructuredGridRepresentation')

# get color transfer function/color map for 'zos_eff'
zos_effLUT = GetColorTransferFunction('zos_eff')
zos_effLUT.ApplyPreset('Inferno (matplotlib)', True)
zos_effLUT.AutomaticRescaleRangeMode = 'Never'
zos_effLUT.NanColor = [0.0, 1.0, 0.0]
zos_effLUT.ScalarRangeInitialized = 1.0
zos_effLUT.RescaleTransferFunction(-4,0)
zos_effLUT.InvertTransferFunction()



# get opacity transfer function/opacity map for 'zos_eff'
zos_effPWF = GetOpacityTransferFunction('zos_eff')
zos_effPWF.Points = [-5.0, 0.0, 0.5, 0.0, 0.0, 1.0, 0.5, 0.0]
zos_effPWF.ScalarRangeInitialized = 1

# trace defaults for the display properties.
zos_effDisplay.Representation = 'Surface'
zos_effDisplay.ColorArrayName = ['CELLS', 'zos_eff']
zos_effDisplay.LookupTable = zos_effLUT
zos_effDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
zos_effDisplay.SelectOrientationVectors = 'None'
zos_effDisplay.ScaleFactor = 62.82254333496094
zos_effDisplay.SelectScaleArray = 'Result'
zos_effDisplay.GlyphType = 'Arrow'
zos_effDisplay.GlyphTableIndexArray = 'Result'
zos_effDisplay.GaussianRadius = 3.141127166748047
zos_effDisplay.SetScaleArray = [None, '']
zos_effDisplay.ScaleTransferFunction = 'PiecewiseFunction'
zos_effDisplay.OpacityArray = [None, '']
zos_effDisplay.OpacityTransferFunction = 'PiecewiseFunction'
zos_effDisplay.DataAxesGrid = 'GridAxesRepresentation'
zos_effDisplay.PolarAxes = 'PolarAxesRepresentation'
zos_effDisplay.ScalarOpacityFunction = zos_effPWF
zos_effDisplay.ScalarOpacityUnitDistance = 2.8552085871048254
zos_effDisplay.BumpMapInputDataArray = [None, '']
zos_effDisplay.ExtrusionInputDataArray = ['CELLS', 'Result']
zos_effDisplay.SelectInputVectors = [None, '']
zos_effDisplay.WriteLog = ''

# show data from psl_contour
psl_contourDisplay = Show(psl_contour, renderView1, 'GeometryRepresentation')

# get color transfer function/color map for 'psl'
pslLUT = GetColorTransferFunction('psl')
pslLUT.AutomaticRescaleRangeMode = 'Never'
pslLUT.RGBPoints = [95000.0, 0.231373, 0.298039, 0.752941, 97000.0, 0.865003, 0.865003, 0.865003, 99000.0, 0.705882, 0.0156863, 0.14902]
pslLUT.NumberOfTableValues = 5
pslLUT.ScalarRangeInitialized = 1.0

# trace defaults for the display properties.
psl_contourDisplay.Representation = 'Surface'
psl_contourDisplay.ColorArrayName = ['POINTS', 'psl']
psl_contourDisplay.LookupTable = pslLUT
psl_contourDisplay.OSPRayScaleArray = 'psl'
psl_contourDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
psl_contourDisplay.SelectOrientationVectors = 'None'
psl_contourDisplay.ScaleFactor = 62.819314575195314
psl_contourDisplay.SelectScaleArray = 'psl'
psl_contourDisplay.GlyphType = 'Arrow'
psl_contourDisplay.GlyphTableIndexArray = 'psl'
psl_contourDisplay.GaussianRadius = 3.140965728759766
psl_contourDisplay.SetScaleArray = ['POINTS', 'psl']
psl_contourDisplay.ScaleTransferFunction = 'PiecewiseFunction'
psl_contourDisplay.OpacityArray = ['POINTS', 'psl']
psl_contourDisplay.OpacityTransferFunction = 'PiecewiseFunction'
psl_contourDisplay.DataAxesGrid = 'GridAxesRepresentation'
psl_contourDisplay.PolarAxes = 'PolarAxesRepresentation'
psl_contourDisplay.BumpMapInputDataArray = ['POINTS', 'psl']
psl_contourDisplay.ExtrusionInputDataArray = ['POINTS', 'psl']
psl_contourDisplay.SelectInputVectors = [None, '']
psl_contourDisplay.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
psl_contourDisplay.ScaleTransferFunction.Points = [99000.0, 0.0, 0.5, 0.0, 105000.0, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
psl_contourDisplay.OpacityTransferFunction.Points = [99000.0, 0.0, 0.5, 0.0, 105000.0, 1.0, 0.5, 0.0]

# setup the color legend parameters for each legend in this view

# get color legend/bar for pslLUT in view renderView1
pslLUTColorBar = GetScalarBar(pslLUT, renderView1)
pslLUTColorBar.WindowLocation = 'UpperLeftCorner'
pslLUTColorBar.Title = 'psl'
pslLUTColorBar.ComponentTitle = ''

# set color bar visibility
pslLUTColorBar.Visibility = 1

# get color legend/bar for zos_effLUT in view renderView1
zos_effLUTColorBar = GetScalarBar(zos_effLUT, renderView1)
zos_effLUTColorBar.WindowLocation = 'UpperRightCorner'
zos_effLUTColorBar.Title = 'zos - 0.9 hi*conc - 0.3 hs*conc'
zos_effLUTColorBar.ComponentTitle = ''

# set color bar visibility
zos_effLUTColorBar.Visibility = 1

# show color legend
zos_effDisplay.SetScalarBarVisibility(renderView1, True)

# show color legend
psl_contourDisplay.SetScalarBarVisibility(renderView1, True)

# ----------------------------------------------------------------
# setup color maps and opacity mapes used in the visualization
# note: the Get..() functions create a new object, if needed
# ----------------------------------------------------------------

# get opacity transfer function/opacity map for 'psl'
pslPWF = GetOpacityTransferFunction('psl')
pslPWF.Points = [95000.0, 0.0, 0.5, 0.0, 99000.0, 1.0, 0.5, 0.0]
pslPWF.ScalarRangeInitialized = 1



# STUFF THAT'S NEW / CHANGED
SetActiveSource(oce_file)

view = GetActiveView()
reader=GetActiveSource()
tsteps=reader.TimestepValues
print ("Timesteps: ", ", ".join((str (x) for x in tsteps)))
for n,t in enumerate (tsteps):
    print ("rendering for time %f"%t)
    view.ViewTime = t
    view.CameraPosition = [0, -160, 10000.0]
    view.CameraFocalPoint = [0, -160., 0.0]
    renderView1.CameraParallelScale = 42.
    SaveScreenshot('%s_%s_4k_%06d.png'%(my_id, "SH", n), ImageResolution=(3840,2160))
    SaveScreenshot('%s_%s_8k_%06d.png'%(my_id, "SH", n), ImageResolution=(7680,4320))
    view.CameraPosition = [0, 160, 15000.0]
    view.CameraFocalPoint = [0, 160, 0.0]
    renderView1.CameraParallelScale = 60.
    SaveScreenshot('%s_%s_4k_%06d.png'%(my_id, "NH", n), ImageResolution=(3840,2160))
    SaveScreenshot('%s_%s_8k_%06d.png'%(my_id, "NH", n), ImageResolution=(7680,4320))

# state file generated using paraview version 5.8.1

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

# trace generated using paraview version 5.8.1
#
# To ensure correct image size when batch processing, please search 
# for and uncomment the line `# renderView*.ViewSize = [*,*]`

#### import the simple module from the paraview
from paraview.simple import *
import run_pv_ao
import os

options=run_pv_ao.parse_args()
my_id="so_conc_"+os.path.basename(options["oce"][0])[:-3].split("_")[-1]
print (my_id)

#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# Create a new 'Render View'
renderView1 = CreateView('RenderView')
renderView1.ViewSize = [1920, 1080]
renderView1.InteractionMode = '2D'
renderView1.AxesGrid = 'GridAxes3DActor'
renderView1.OrientationAxesVisibility = 0
renderView1.CenterOfRotation = [0.001556396484375, 9.228599548339844, 0.0]
renderView1.UseLight = 0
renderView1.StereoType = 'Crystal Eyes'
renderView1.CameraPosition = [5.460894817615681, -150.2376559564407, 10000.0]
renderView1.CameraFocalPoint = [5.460894817615681, -150.2376559564407, 0.0]
renderView1.CameraFocalDisk = 1.0
renderView1.CameraParallelScale = 60.20944096671894

SetActiveView(None)

# ----------------------------------------------------------------
# setup view layouts
# ----------------------------------------------------------------

# create new layout object 'Layout #1'
layout1 = CreateLayout(name='Layout #1')
layout1.AssignView(0, renderView1)

# ----------------------------------------------------------------
# restore active view
SetActiveView(renderView1)
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------

# create a new 'CDIReader'
ice_file = CDIReader(FileNames= options['ice'])
ice_file.Dimensions = '(clon, clat, lev)'
ice_file.CellArrayStatus = ['hi', 'hs', 'conc']
ice_file.SetProjection = 'Cassini Projection'
ice_file.LayerThickness = 50

# create a new 'NetCDF Time Annotation'
netCDFTimeAnnotation1 = NetCDFTimeAnnotation(Input=ice_file)
netCDFTimeAnnotation1.Expression = '"%02i-%02i-%02i" % (Date[0], Date[1], Date[2])'

# create a new 'CDIReader'
oce_file = CDIReader(FileNames=options['oce'])
oce_file.Dimensions = '(clon, clat, depth)'
oce_file.CellArrayStatus = ['to', 'so']
oce_file.SetProjection = 'Cassini Projection'
oce_file.LayerThickness = 50
oce_file.VerticalLevel = 0

# create a new 'Threshold'
so_threshold = Threshold(Input=oce_file)
so_threshold.Scalars = ['CELLS', 'so']
so_threshold.ThresholdRange = [0.00001, 60.0]

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# show data from ice_file
ice_fileDisplay = Show(ice_file, renderView1, 'UnstructuredGridRepresentation')

# get color transfer function/color map for 'conc'
concLUT = GetColorTransferFunction('conc')
concLUT.EnableOpacityMapping = 1
concLUT.RGBPoints = [0.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0]
concLUT.ColorSpace = 'RGB'
concLUT.ScalarRangeInitialized = 1.0

# get opacity transfer function/opacity map for 'conc'
concPWF = GetOpacityTransferFunction('conc')
concPWF.Points = [0.0, 0.0, 0.5, 0.0, 0.15, 0.5, 0.5, 0.0, 1.0, 0.5, 0.5, 0.0]
concPWF.ScalarRangeInitialized = 1

# trace defaults for the display properties.
ice_fileDisplay.Representation = 'Surface'
ice_fileDisplay.ColorArrayName = ['CELLS', 'conc']
ice_fileDisplay.LookupTable = concLUT
ice_fileDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
ice_fileDisplay.SelectOrientationVectors = 'None'
ice_fileDisplay.ScaleFactor = 60.166174316406256
ice_fileDisplay.SelectScaleArray = 'None'
ice_fileDisplay.GlyphType = 'Arrow'
ice_fileDisplay.GlyphTableIndexArray = 'None'
ice_fileDisplay.GaussianRadius = 3.008308715820313
ice_fileDisplay.SetScaleArray = [None, '']
ice_fileDisplay.ScaleTransferFunction = 'PiecewiseFunction'
ice_fileDisplay.OpacityArray = [None, '']
ice_fileDisplay.OpacityTransferFunction = 'PiecewiseFunction'
ice_fileDisplay.DataAxesGrid = 'GridAxesRepresentation'
ice_fileDisplay.PolarAxes = 'PolarAxesRepresentation'
ice_fileDisplay.ScalarOpacityFunction = concPWF
ice_fileDisplay.ScalarOpacityUnitDistance = 6.795499595074316
ice_fileDisplay.BumpMapInputDataArray = [None, '']
ice_fileDisplay.ExtrusionInputDataArray = ['CELLS', 'hi']
ice_fileDisplay.SelectInputVectors = [None, '']
ice_fileDisplay.WriteLog = ''

# show data from so_threshold
so_thresholdDisplay = Show(so_threshold, renderView1, 'UnstructuredGridRepresentation')

# get color transfer function/color map for 'so'
soLUT = GetColorTransferFunction('so')
soLUT.AutomaticRescaleRangeMode = 'Never'
soLUT.RGBPoints = [20.000000000000004, 0.278431372549, 0.278431372549, 0.858823529412, 24.0, 0.0, 0.0, 0.360784313725, 28.0, 0.0, 0.01568627450980392, 0.37254901960784315, 32.0, 0.0, 1.0, 1.0, 33.0, 0.0, 0.501960784314, 0.0, 33.5, 1.0, 1.0, 0.0, 34.0, 1.0, 0.380392156863, 0.0, 34.5, 0.419607843137, 0.0, 0.0, 35.0, 0.878431372549, 0.301960784314, 0.301960784314]
soLUT.ColorSpace = 'RGB'
soLUT.ScalarRangeInitialized = 1.0

# get opacity transfer function/opacity map for 'so'
soPWF = GetOpacityTransferFunction('so')
soPWF.Points = [20.0, 0.0, 0.5, 0.0, 35.0, 1.0, 0.5, 0.0]
soPWF.ScalarRangeInitialized = 1

# trace defaults for the display properties.
so_thresholdDisplay.Representation = 'Surface'
so_thresholdDisplay.ColorArrayName = ['CELLS', 'so']
so_thresholdDisplay.LookupTable = soLUT
so_thresholdDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
so_thresholdDisplay.SelectOrientationVectors = 'None'
so_thresholdDisplay.ScaleFactor = 62.79500427246094
so_thresholdDisplay.SelectScaleArray = 'None'
so_thresholdDisplay.GlyphType = 'Arrow'
so_thresholdDisplay.GlyphTableIndexArray = 'None'
so_thresholdDisplay.GaussianRadius = 3.139750213623047
so_thresholdDisplay.SetScaleArray = [None, '']
so_thresholdDisplay.ScaleTransferFunction = 'PiecewiseFunction'
so_thresholdDisplay.OpacityArray = [None, '']
so_thresholdDisplay.OpacityTransferFunction = 'PiecewiseFunction'
so_thresholdDisplay.DataAxesGrid = 'GridAxesRepresentation'
so_thresholdDisplay.PolarAxes = 'PolarAxesRepresentation'
so_thresholdDisplay.ScalarOpacityFunction = soPWF
so_thresholdDisplay.ScalarOpacityUnitDistance = 7.210946129682988
so_thresholdDisplay.BumpMapInputDataArray = [None, '']
so_thresholdDisplay.ExtrusionInputDataArray = ['CELLS', 'Center Latitude (CLAT)']
so_thresholdDisplay.SelectInputVectors = [None, '']
so_thresholdDisplay.WriteLog = ''

# show data from netCDFTimeAnnotation1
netCDFTimeAnnotation1Display = Show(netCDFTimeAnnotation1, renderView1, 'TextSourceRepresentation')

# trace defaults for the display properties.
netCDFTimeAnnotation1Display.FontSize = 12
netCDFTimeAnnotation1Display.WindowLocation = 'UpperRightCorner'
netCDFTimeAnnotation1Display.Position = [0.01, 0.9196296296296296]

# setup the color legend parameters for each legend in this view

# get color legend/bar for soLUT in view renderView1
soLUTColorBar = GetScalarBar(soLUT, renderView1)
soLUTColorBar.Position = [0.9395833333333333, 0.011111111111111112]
soLUTColorBar.Title = 'so'
soLUTColorBar.ComponentTitle = ''

# set color bar visibility
soLUTColorBar.Visibility = 1

# show color legend
so_thresholdDisplay.SetScalarBarVisibility(renderView1, True)

# ----------------------------------------------------------------
# setup color maps and opacity mapes used in the visualization
# note: the Get..() functions create a new object, if needed
# ----------------------------------------------------------------

# ----------------------------------------------------------------
SetActiveSource(netCDFTimeAnnotation1)
# ----------------------------------------------------------------


SetActiveSource(oce_file)

view = GetActiveView()
reader=GetActiveSource()
tsteps=reader.TimestepValues
print ("Timesteps: ", ", ".join((str (x) for x in tsteps)))
if len(tsteps) == 0 :
    tsteps = [0]
for n,t in enumerate (tsteps):
    print ("rendering for time %f"%t)
    view.ViewTime = t
    view.CameraPosition = [0, -160, 10000.0]
    view.CameraFocalPoint = [0, -160., 0.0]
    renderView1.CameraParallelScale = 42.
    filename='%s_%s_4k_%06d.png'%(my_id, "SH", n)
    print("saving to ", filename)
    SaveScreenshot(filename, ImageResolution=(3840,2160))
    SaveScreenshot('%s_%s_8k_%06d.png'%(my_id, "SH", n), ImageResolution=(7680,4320))
    SaveScreenshot('%s_%s_hd_%06d.png'%(my_id, "SH", n), ImageResolution=(1920,1080))
    view.CameraPosition = [0, 160, 15000.0]
    view.CameraFocalPoint = [0, 160, 0.0]
    renderView1.CameraParallelScale = 60.
    SaveScreenshot('%s_%s_4k_%06d.png'%(my_id, "NH", n), ImageResolution=(3840,2160))
    SaveScreenshot('%s_%s_hd_%06d.png'%(my_id, "NH", n), ImageResolution=(1920,1080))
    SaveScreenshot('%s_%s_8k_%06d.png'%(my_id, "NH", n), ImageResolution=(7680,4320))

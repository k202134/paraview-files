from paraview.simple import Show, Transform, GetColorTransferFunction, GetOpacityTransferFunction, GetActiveViewOrCreate, CDIReader
def plot_snow (files,  scale=1.01):

    print ("plot_snow.plot_snow starting for ", files)
    renderView1 = GetActiveViewOrCreate('RenderView')
    # create a new 'CDIReader'
    print ("Opening file(s)")
    reader = CDIReader(registrationName='snow', FileNames=files)
    reader.Dimensions = '(lon, lat, sfc)'
    reader.CellArrayStatus = ['hydro_w_snow_box']
    reader.SetProjection = 'Spherical Projection'


    snow = Show(reader, renderView1, 'UnstructuredGridRepresentation')
    
    # get color transfer function/color map for 'conc'
    snowLUT = GetColorTransferFunction('hydro_w_snow_box')
    snowLUT.EnableOpacityMapping = 1
    snowLUT.RGBPoints = [0.0, 1.0, 1.0, 1.0, 0.05, 1.0, 1.0, 1.0]
    snowLUT.ScalarRangeInitialized = 1.0

    # get opacity transfer function/opacity map for 'snow'
    snowPWF = GetOpacityTransferFunction('hydro_w_snow_box')
    snowPWF.ScalarRangeInitialized = 1

    # trace defaults for the display properties.
    snow.Representation = 'Surface'
    snow.Scale = [scale, scale, scale]
    snow.ColorArrayName = ['CELLS', 'hydro_w_snow_box']
    snow.LookupTable = snowLUT
    snow.SelectTCoordArray = 'None'
    snow.SelectNormalArray = 'None'
    snow.SelectTangentArray = 'None'
    snow.OSPRayScaleFunction = 'PiecewiseFunction'
    snow.SelectOrientationVectors = 'None'
    snow.ScaleFactor = 59.99990539550782
    snow.SelectScaleArray = 'None'
    snow.GlyphType = 'Arrow'
    snow.GlyphTableIndexArray = 'None'
    snow.GaussianRadius = 2.9999952697753907
    snow.SetScaleArray = [None, '']
    snow.ScaleTransferFunction = 'PiecewiseFunction'
    snow.OpacityArray = [None, '']
    snow.OpacityTransferFunction = 'PiecewiseFunction'
    snow.DataAxesGrid = 'GridAxesRepresentation'
    snow.PolarAxes = 'PolarAxesRepresentation'
    snow.ScalarOpacityFunction = snowPWF
    snow.ScalarOpacityUnitDistance = 4.272590800397543
    snow.OpacityArrayName = ['CELLS', 'hydro_w_snow_box']
    snow.Ambient = 0.2
    snow.SpecularPower = 70.0
    snow.Diffuse = 0.5


    return (reader, snow)

from paraview.simple import Show, Transform, GetColorTransferFunction, GetOpacityTransferFunction, GetActiveViewOrCreate, CDIReader
def plot_sea_ice (oce=None, files=None, use_transform=True, scale=1.01):

    my_oce = oce
    if files is not None:
        my_oce = CDIReader(registrationName='ocean', FileNames=files)
        my_oce.Dimensions = '(lon, lat, sfc)'
        my_oce.CellArrayStatus = ['conc']
        my_oce.SetProjection = 'Spherical Projection'



    if (use_transform):
        my_oce = Transform(registrationName='oce', Input=oce)
        my_oce.Transform = 'Transform'
        # init the 'Transform' selected for 'Transform'
        my_oce.Transform.Scale = [scale, scale, scale]
    
    renderView1 = GetActiveViewOrCreate('RenderView')

    conc = Show(my_oce, renderView1, 'UnstructuredGridRepresentation')
    if (not use_transform):
        conc.PolarAxes.Scale = [scale, scale, scale]

    # get color transfer function/color map for 'conc'
    concLUT = GetColorTransferFunction('conc')
    concLUT.EnableOpacityMapping = 1
    concLUT.RGBPoints = [0.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0]
    concLUT.ScalarRangeInitialized = 1.0

    # get opacity transfer function/opacity map for 'conc'
    concPWF = GetOpacityTransferFunction('conc')
    concPWF.ScalarRangeInitialized = 1

    # trace defaults for the display properties.
    conc.Representation = 'Surface'
    conc.ColorArrayName = ['CELLS', 'conc']
    conc.LookupTable = concLUT
    conc.SelectTCoordArray = 'None'
    conc.SelectNormalArray = 'None'
    conc.SelectTangentArray = 'None'
    conc.OSPRayScaleFunction = 'PiecewiseFunction'
    conc.SelectOrientationVectors = 'None'
    conc.ScaleFactor = 59.99990539550782
    conc.SelectScaleArray = 'None'
    conc.GlyphType = 'Arrow'
    conc.GlyphTableIndexArray = 'None'
    conc.GaussianRadius = 2.9999952697753907
    conc.SetScaleArray = [None, '']
    conc.ScaleTransferFunction = 'PiecewiseFunction'
    conc.OpacityArray = [None, '']
    conc.OpacityTransferFunction = 'PiecewiseFunction'
    conc.DataAxesGrid = 'GridAxesRepresentation'
    conc.PolarAxes = 'PolarAxesRepresentation'
    conc.ScalarOpacityFunction = concPWF
    conc.ScalarOpacityUnitDistance = 4.272590800397543
    conc.OpacityArrayName = ['CELLS', 'conc']
    conc.Ambient = 0.2
    conc.SpecularPower = 70.0
    conc.Diffuse = 0.5


    return my_oce

from paraview.simple import ProgrammableFilter, ProgrammableAnnotation, Show, GetActiveViewOrCreate

header = """
from paraview.simple import GetActiveViewOrCreate, GetLight, AddLight

try:
    import cftime as ct
except:
    try:
        import netcdftime as ct
    except:
        print("Need the python cftime (or the older netcdftime) module for the NetCDFTimeAnnotation plugin!", file=sys.stderr)

import datetime as dt
import numpy as np

inp = self.GetInputDataObject(0,0)
currentTime = inp.GetInformation().Get(vtk.vtkDataObject.DATA_TIME_STEP())
sdate=vtk.vtkStringArray()
timeUnitsArray= inp.GetFieldData().GetAbstractArray("time_units")
if timeUnitsArray:
    timeUnits = timeUnitsArray.GetValue(0)
    print ("Time units ", timeUnits)
cdftime = ct.utime(timeUnits)
t = cdftime.num2date(currentTime)
print ("currentTime ", currentTime, ", t = ", t)
first = ct.JulianDayFromDate(dt.datetime(t.year,1,1,0,0,0))
jd =  (ct.JulianDayFromDate(t) - first + 1)

angle = 23.45 *np.pi/180 * np.sin(2 * np.pi * (284 + jd ) /365.25)

time = jd-np.floor(jd)
"""


set_light_acc_to_xyz="""
z = np.tan(angle)
print (jd, time, x, y , z)
renderView1 = GetActiveViewOrCreate(\'RenderView\')
# Create a new \'Light\'
light = GetLight(0, view=renderView1)
if light is None:
    light = AddLight(view=renderView1)
dist = 1e6
light.Position = [x*dist, y*dist, z*dist]
light.DiffuseColor = [1.0, 1.0, 1.0]"""



def adjust_light_to_time(time_source, fixed_time=None):
    print ("Adjusting light")
    lightadjust = ProgrammableFilter(registrationName='light-adjust', Input=time_source)
    lightadjust.OutputDataSetType = 'vtkTable'

    center="""
    x = - np.cos(time * 2 * np.pi)
    y = np.sin(time * 2 * np.pi)
    """
    if fixed_time is not None :
        center = f"""
        x = - np.cos({fixed_time} * 2 * np.pi)
        y = np.sin({fixed_time} * 2 * np.pi)
        """
    
    lightadjust.Script = header + center + set_light_acc_to_xyz
    lightadjust.RequestInformationScript = ''
    lightadjust.RequestUpdateExtentScript = ''
    lightadjust.PythonPath = ''

    # create a new 'Programmable Annotation'
    add_dummy_label( lightadjust, renderView1)
    print ("Done adjusting light")

def add_dummy_label(lightadjust, renderView1):
    dummylabel = ProgrammableAnnotation(registrationName='dummy-label', Input=lightadjust)
    dummylabel.Script = """to = self.GetTableOutput()
arr = vtk.vtkStringArray()
arr.SetName("Text")
arr.SetNumberOfComponents(1)
arr.InsertNextValue("")
to.AddColumn(arr)
"""
    dummylabel.PythonPath = ''
    dummylabelDisplay = Show(dummylabel, renderView1, 'TextSourceRepresentation')
    

def adjust_light_to_calendar(time_source):
    print ("Adjusting light")
    renderView1 = GetActiveViewOrCreate('RenderView')
    lightadjust = ProgrammableFilter(registrationName='light-adjust', Input=time_source)
    lightadjust.OutputDataSetType = 'vtkTable'

    adjustments = """
renderView1 = GetActiveViewOrCreate(\'RenderView\')
renderView1.KeyLightWarmth = 0.5
renderView1.KeyLightElevation = angle * 180 / np.pi
renderView1.KeyLightAzimuth = 0.0
renderView1.FillLightWarmth = 0.3
renderView1.FillLightKFRatio = 4
renderView1.FillLightElevation = -90.0
renderView1.FillLightAzimuth = 0.0
renderView1.BackLightKBRatio = 4
renderView1.BackLightAzimuth = 0.0
renderView1.BackLightElevation = 90.0
renderView1.BackLightWarmth = 0.3
renderView1.HeadLightKHRatio = 2.5
renderView1.KeyLightIntensity = 0.8
"""
    
    lightadjust.Script = header + adjustments
    lightadjust.RequestInformationScript = ''
    lightadjust.RequestUpdateExtentScript = ''
    lightadjust.PythonPath = ''

    # create a new 'Programmable Annotation'
    add_dummy_label( lightadjust, renderView1)
    print ("Done adjusting light")
    

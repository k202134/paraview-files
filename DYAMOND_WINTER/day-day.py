# state file generated using paraview version 5.8.1-2374-gdd080cd

#### import the simple module from the paraview
from paraview.simple import *
import sys
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

print (sys.argv)


from argparse import ArgumentParser
import glob

r = 126400.
def sphere_proj (lat, lon) :
    global r
    theta = (90 - lat) * pi / 180.
    phi = lon * pi / 180.
    z = r * cos (theta)
    x = r * sin(theta) * cos (phi)
    y = r * sin(theta) * sin (phi)
    return (x, y, z)

options = {}
def parse_args():
    '''Parses the command line arguments'''
    global options
    parser = ArgumentParser()
    parser.description = "Plot icon files"
    parser.add_argument("--clouds")
    parser.add_argument("--cloud_expr")
    parser.add_argument("--cloud_vars")
    parser.add_argument("--psl",)
    parser.add_argument("--psl_var",)
    parser.add_argument("--light")
    parser.add_argument("--lonlat", nargs = 2, type=float)
    parser.add_argument("--output", "-o")
    parser.add_argument("--no_time_string", action="store_true")
    op = parser.parse_args()
    options = vars(op)
    print (options)
    if not options.get("cloud_vars", False) :
        options["cloud_vars"] = "cloud"
    options["cloud_vars"] = options["cloud_vars"].split(",")

    if not options.get("output", False) :
        options["output"] = options["clouds"][:-3]

    if not options.get("psl_var", False) :
        options["psl_var"] = "psl"

    print (options)
    return options

options=parse_args()




# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

# Create a new 'Light'

# Create a new 'Light'
light2 = CreateLight()
light2.Coords = 'Ambient'
light2.Intensity = 0.9
light2.Position = [0.0, -100.0, 0.0]
light2.DiffuseColor = [1.0, 1.0, 1.0 ]

# Create a new 'Light'
light3 = CreateLight()
light3.Enable = 0
light3.Position = [562.3115794309297, -528.4831081310286, 194.20829319495272]
light3.FocalPoint = [562.3058234478694, -528.4776977247036, 194.2062917442533]

# get the material library
materialLibrary1 = GetMaterialLibrary()

# create light
# create light
# create light
# Create a new 'Render View'
renderView1 = CreateView('RenderView')
renderView1.ViewSize = [1594, 1233]
renderView1.AxesGrid = 'GridAxes3DActor'
renderView1.OrientationAxesVisibility = 0
renderView1.KeyLightIntensity = 0.0
renderView1.StereoType = 'Crystal Eyes'
renderView1.CameraPosition = [1144.0024360966738, -682.5869061407763, 142.4868048724324]
renderView1.CameraViewUp = [0, 0., 1]
renderView1.CameraViewAngle = 20. * 1264. / r
renderView1.CameraFocalDisk = 1.0
renderView1.CameraParallelScale = 564.2333216713644
renderView1.EyeAngle = 1.0
renderView1.Background = [1.0, 1.0, 1.0]
renderView1.BackEnd = 'OSPRay raycaster'
renderView1.EnvironmentNorth = [0.0, 0.0, 0.0]
renderView1.AdditionalLights = [light2, light3]
renderView1.OSPRayMaterialLibrary = materialLibrary1

SetActiveView(None)

# ----------------------------------------------------------------
# setup view layouts
# ----------------------------------------------------------------

# create new layout object 'Layout #1'
layout1 = CreateLayout(name='Layout #1')
layout1.AssignView(0, renderView1)
layout1.SetSize(1594, 1233)

# ----------------------------------------------------------------
# restore active view
SetActiveView(renderView1)
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------
fn = options["clouds"]

if "UM" in options["clouds"] or "SAM" in options["clouds"] or "NICAM" in options["clouds"]:
    if ":" in fn:
        files = fn.split(":")
        readers = [ NetCDFReader(registrationName='cloud_psl_%d'%n, FileName=x.split(",")) for n, x in enumerate( files ) ]
        for x in readers:
            x.Dimensions = '(latitude, longitude)' 
            if "NICAM" in options["clouds"]:
                x.Dimensions = '(lat, lon)'
            x.VerticalScale = 200.0
        cloud_psl = AppendAttributes(registrationName='cloud_psl', Input=readers )
    else:
        cloud_psl = NetCDFReader(registrationName='cloud_psl', FileName=options["clouds"].split(","))
        cloud_psl.VerticalScale = 200.0
else:
    if ":" in fn:
        files = fn.split(":")
        readers = [ CDIReader(registrationName='cloud_psl_%d'%n, FileNames=x.split(",")) for n, x in enumerate( files ) ]
        rxx = [ print(x.split(",")) for n, x in enumerate( files ) ]
        for n, x in enumerate(readers):
            x.Dimensions = '(clon, clat, sfc)' 
            x.CellArrayStatus = [options['cloud_vars'][n]]
            x.SetProjection = 'Spherical Projection' 
            x.LayerThickness = 50 
        cloud_psl = AppendAttributes(registrationName='cloud_psl', Input=readers )

    else:
        # create a new 'CDIReader'
        cloud_psl = CDIReader(registrationName='cloud_psl', FileNames=options["clouds"].split(","))
        print (options["clouds"].split(","))
        cloud_psl.Dimensions = '(clon, clat, sfc)'
        cloud_psl.CellArrayStatus = options['cloud_vars']
        cloud_psl.SetProjection = 'Spherical Projection'
        cloud_psl.LayerThickness = 50
        print (dir(cloud_psl))
        cloud_psl.SkipGrid=True

if options.get("psl", False):
    print ("Getting psl", file=sys.stderr)
    psl = CDIReader(registrationName='psl', FileNames=options["psl"].split(","))
    psl.Dimensions = '(clon, clat, sfc)'
    psl.CellArrayStatus = [options['psl_var']]
    psl.SetProjection = 'Spherical Projection'
    psl.LayerThickness = 50
    psl.MaskingValueVar = 'psl'
    # create a new 'Cell Data to Point Data'
    psl_to_point = CellDatatoPointData(registrationName='psl_to_point', Input=psl)
    psl_to_point.ProcessAllArrays = 0
    psl_to_point.CellDataArraytoprocess = [options['psl_var']]

    # create a new 'Contour'
    psl_contour = Contour(registrationName='psl_contour', Input=psl_to_point)
    psl_contour.ContourBy = ['POINTS', options['psl_var'] ]
    psl_contour.Isosurfaces = [99000.0, 100500.0, 102000.0, 103500.0]
    psl_contour.PointMergeMethod = 'Uniform Binning'
    print ("Done getting psl", file=sys.stderr)




# create a new 'Calculator'
alpha = Calculator(registrationName='alpha', Input=cloud_psl)
alpha.AttributeType = 'Cell Data'
alpha.ResultArrayName = 'alpha'
cloud_expr = options.get("cloud_expr", False)
if not cloud_expr:
    cloud_expr = " ( 5* clivi + clwvi ) "
alpha.Function = ' (3/2 * ({cloud_expr}) *100)/ (3/2 * ({cloud_expr}) *100+7)'.format(cloud_expr=cloud_expr)


# create a new 'NetCDF Time Annotation'
netCDFTimeAnnotation1 = NetCDFTimeAnnotation(registrationName='NetCDFTimeAnnotation1', Input=cloud_psl)
netCDFTimeAnnotation1.Expression = '"%02i-%02i-%02i %02i:%02i" % (Date[0], Date[1], Date[2], Date[3], Date[4])'

if not options.get("no_time_string", False):
    # show data in view
    netCDFTimeAnnotation1Display = Show(netCDFTimeAnnotation1, renderView1, 'TextSourceRepresentation')
    netCDFTimeAnnotation1Display.FontSize = 96
    netCDFTimeAnnotation1Display.FontFamily = 'Courier'




# create a new 'Texture Map to Sphere'
day = TextureMaptoSphere(registrationName='day', Input=cloud_psl)
day.PreventSeam = 0

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# show data from day
dayDisplay = Show(day, renderView1, 'UnstructuredGridRepresentation')

# get separate color transfer function/color map for 'rsdt'
separate_dayDisplay_rsdtLUT = GetColorTransferFunction('rsdt', dayDisplay, separate=True)
separate_dayDisplay_rsdtLUT.AutomaticRescaleRangeMode = 'Never'
separate_dayDisplay_rsdtLUT.EnableOpacityMapping = 1
separate_dayDisplay_rsdtLUT.RGBPoints = [0.0, 1.0, 1.0, 1.0, 10.0, 1.0, 1.0, 1.0]
separate_dayDisplay_rsdtLUT.ScalarRangeInitialized = 1.0

# a texture
world2 = CreateTexture('/home/k/k202134/world.200408.3x5400x2700_for_pv.jpg')

# get separate opacity transfer function/opacity map for 'rsdt'
separate_dayDisplay_rsdtPWF = GetOpacityTransferFunction('rsdt', dayDisplay, separate=True)
separate_dayDisplay_rsdtPWF.Points = [0.0, 0.0, 0.5, 0.0, 10.0, 1.0, 0.5, 0.0]
separate_dayDisplay_rsdtPWF.ScalarRangeInitialized = 1

# trace defaults for the display properties.
dayDisplay.Representation = 'Surface'
dayDisplay.ColorArrayName = [None, '']
dayDisplay.LookupTable = separate_dayDisplay_rsdtLUT
dayDisplay.SelectTCoordArray = 'Texture Coordinates'
dayDisplay.SelectNormalArray = 'None'
dayDisplay.SelectTangentArray = 'None'
dayDisplay.Texture = world2
dayDisplay.OSPRayScaleArray = 'Texture Coordinates'
dayDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
dayDisplay.SelectOrientationVectors = 'None'
dayDisplay.ScaleFactor = 40.0
dayDisplay.SelectScaleArray = 'None'
dayDisplay.GlyphType = 'Arrow'
dayDisplay.GlyphTableIndexArray = 'None'
dayDisplay.GaussianRadius = 2.0
dayDisplay.SetScaleArray = ['POINTS', 'Texture Coordinates']
dayDisplay.ScaleTransferFunction = 'PiecewiseFunction'
dayDisplay.OpacityArray = ['POINTS', 'Texture Coordinates']
dayDisplay.OpacityTransferFunction = 'PiecewiseFunction'
dayDisplay.DataAxesGrid = 'GridAxesRepresentation'
dayDisplay.PolarAxes = 'PolarAxesRepresentation'
dayDisplay.ScalarOpacityFunction = separate_dayDisplay_rsdtPWF
dayDisplay.ScalarOpacityUnitDistance = 2.329517136805135
dayDisplay.OpacityArrayName = ['POINTS', 'Texture Coordinates']
dayDisplay.BumpMapInputDataArray = [None, '']
dayDisplay.ExtrusionInputDataArray = ['POINTS', 'Texture Coordinates']
dayDisplay.SelectInputVectors = ['POINTS', 'Texture Coordinates']
dayDisplay.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
dayDisplay.ScaleTransferFunction.Points = [4.875790182268247e-05, 0.0, 0.5, 0.0, 0.9999512434005737, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
dayDisplay.OpacityTransferFunction.Points = [4.875790182268247e-05, 0.0, 0.5, 0.0, 0.9999512434005737, 1.0, 0.5, 0.0]

# set separate color map
dayDisplay.UseSeparateColorMap = True


print ("Done setting up textures", file=sys.stderr)

print ("Activating clouds", file=sys.stderr)

# show data from alpha
alphaDisplay = Show(alpha, renderView1, 'UnstructuredGridRepresentation')
print ("Done activating clouds", file=sys.stderr)

print ("Setting up clouds", file=sys.stderr)
# get color transfer function/color map for 'alpha'
alphaLUT = GetColorTransferFunction('alpha')
alphaLUT.AutomaticRescaleRangeMode = 'Never'
alphaLUT.EnableOpacityMapping = 1
alphaLUT.RGBPoints = [0.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0]
alphaLUT.ColorSpace = 'RGB'
alphaLUT.ScalarRangeInitialized = 1.0

# get opacity transfer function/opacity map for 'alpha'
alphaPWF = GetOpacityTransferFunction('alpha')
alphaPWF.ScalarRangeInitialized = 1

# trace defaults for the display properties.
alphaDisplay.Representation = 'Surface'
alphaDisplay.ColorArrayName = ['CELLS', 'alpha']
alphaDisplay.LookupTable = alphaLUT
alphaDisplay.SelectTCoordArray = 'None'
alphaDisplay.SelectNormalArray = 'None'
alphaDisplay.SelectTangentArray = 'None'
alphaDisplay.Scale = [1.001, 1.001, 1.001]
alphaDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
alphaDisplay.SelectOrientationVectors = 'None'
alphaDisplay.ScaleFactor = 40.0
alphaDisplay.SelectScaleArray = 'alpha'
alphaDisplay.GlyphType = 'Arrow'
alphaDisplay.GlyphTableIndexArray = 'alpha'
alphaDisplay.GaussianRadius = 2.0
alphaDisplay.SetScaleArray = [None, '']
alphaDisplay.ScaleTransferFunction = 'PiecewiseFunction'
alphaDisplay.OpacityArray = [None, '']
alphaDisplay.OpacityTransferFunction = 'PiecewiseFunction'
alphaDisplay.DataAxesGrid = 'GridAxesRepresentation'
alphaDisplay.PolarAxes = 'PolarAxesRepresentation'
alphaDisplay.ScalarOpacityFunction = alphaPWF
alphaDisplay.ScalarOpacityUnitDistance = 2.5123335565563587
alphaDisplay.OpacityArrayName = ['CELLS', 'alpha']
alphaDisplay.BumpMapInputDataArray = [None, '']
alphaDisplay.ExtrusionInputDataArray = ['CELLS', 'alpha']
alphaDisplay.SelectInputVectors = ['CELLS', 'alpha']
alphaDisplay.WriteLog = ''

# init the 'PolarAxesRepresentation' selected for 'PolarAxes'
alphaDisplay.PolarAxes.Scale = [1.001, 1.001, 1.001]

print ("Done setting up clouds", file=sys.stderr)

print ("Activating textures", file=sys.stderr)




print ("Done activating textures", file=sys.stderr)


if options.get("psl", False):
    print ("Displaying psl", file=sys.stderr)
    # show data from psl_contour
    psl_contourDisplay = Show(psl_contour, renderView1, 'GeometryRepresentation')
    print ("... initialized ... ", file=sys.stderr)

    # get color transfer function/color map for 'psl'
    pslLUT = GetColorTransferFunction(options['psl_var'])
    pslLUT.AutomaticRescaleRangeMode = 'Never'
    pslLUT.RGBPoints = [100000.0, 0.23137254902, 0.298039215686, 0.752941176471, 101250.0, 0.865, 0.865, 0.865, 102500.0, 0.705882352941, 0.0156862745098, 0.149019607843]
    pslLUT.ScalarRangeInitialized = 1.0

    # trace defaults for the display properties.
    psl_contourDisplay.Representation = 'Surface'
    psl_contourDisplay.ColorArrayName = ['POINTS', options['psl_var']]
    psl_contourDisplay.LookupTable = pslLUT
    psl_contourDisplay.LineWidth = 4.0
    psl_contourDisplay.SelectTCoordArray = 'None'
    psl_contourDisplay.SelectNormalArray = 'None'
    psl_contourDisplay.SelectTangentArray = 'None'
    psl_contourDisplay.Scale = [1.001, 1.001, 1.001]
    psl_contourDisplay.OSPRayScaleArray = options['psl_var']
    psl_contourDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
    psl_contourDisplay.SelectOrientationVectors = 'None'
    psl_contourDisplay.ScaleFactor = 39.99499206542969
    psl_contourDisplay.SelectScaleArray = options['psl_var']
    psl_contourDisplay.GlyphType = 'Arrow'
    psl_contourDisplay.GlyphTableIndexArray = options['psl_var']
    psl_contourDisplay.GaussianRadius = 1.9997496032714843
    psl_contourDisplay.SetScaleArray = ['POINTS', options['psl_var']]
    psl_contourDisplay.ScaleTransferFunction = 'PiecewiseFunction'
    psl_contourDisplay.OpacityArray = ['POINTS', options['psl_var']]
    psl_contourDisplay.OpacityTransferFunction = 'PiecewiseFunction'
    psl_contourDisplay.DataAxesGrid = 'GridAxesRepresentation'
    psl_contourDisplay.PolarAxes = 'PolarAxesRepresentation'
    psl_contourDisplay.BumpMapInputDataArray = ['POINTS', options['psl_var']]
    psl_contourDisplay.ExtrusionInputDataArray = ['POINTS', options['psl_var']]
    psl_contourDisplay.SelectInputVectors = ['POINTS', options['psl_var']]
    psl_contourDisplay.WriteLog = ''

    # init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
    psl_contourDisplay.ScaleTransferFunction.Points = [99000.0, 0.0, 0.5, 0.0, 103500.0, 1.0, 0.5, 0.0]

    # init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
    psl_contourDisplay.OpacityTransferFunction.Points = [99000.0, 0.0, 0.5, 0.0, 103500.0, 1.0, 0.5, 0.0]

    # init the 'PolarAxesRepresentation' selected for 'PolarAxes'
    psl_contourDisplay.PolarAxes.Scale = [1.001, 1.001, 1.001]

    # ----------------------------------------------------------------
    # setup color maps and opacity mapes used in the visualization
    # note: the Get..() functions create a new object, if needed
    # ----------------------------------------------------------------

    # get opacity transfer function/opacity map for options['psl_var']
    pslPWF = GetOpacityTransferFunction(options['psl_var'])
    pslPWF.Points = [100000.0, 0.0, 0.5, 0.0, 102500.0, 1.0, 0.5, 0.0]
    pslPWF.ScalarRangeInitialized = 1
    print ("Done displaying psl", file=sys.stderr)

# ----------------------------------------------------------------
# restore active source
SetActiveSource(cloud_psl)
# ----------------------------------------------------------------


if "SHiELD" in options["clouds"] :
    psl_contour.Isosurfaces = [ x / 100. for x in psl_contour.Isosurfaces ]
    pslLUT.RGBPoints = [1000.0, 0.23137254902, 0.298039215686, 0.752941176471, 1012.500, 0.865, 0.865, 0.865, 1025.0, 0.705882352941, 0.0156862745098, 0.149019607843]

alphaLUT.RescaleTransferFunction(0.0, 1.0)
alphaPWF.RescaleTransferFunction(0.0, 1.0)
if __name__ == '__main__':
    view = GetActiveView()

    oldpos = view.CameraPosition
    
    print ('in main', file=sys.stderr)
    reader=GetActiveSource()
    tsteps=reader.TimestepValues
    print ("Timesteps: ", ", ".join((str (x) for x in tsteps)))
    def genname(fn, n, atts):
        attstr = "_".join((x for x in atts if x))
        return "{fn}_{atts}_{n:03d}.png".format(fn=fn, n=n, atts=attstr)
    for n,t in enumerate (tsteps):
        print ("rendering for time %f"%t)
        if len (tsteps) > 1:
            view.ViewTime = t

        # generate extracts
        # renderView1.Background = [1.0, 1.0, 1.0]
        # SaveScreenshot(genname (options["output"], n, [ll_string, "white", "HD"]), renderView1, ImageResolution=[1920, 1080])
        # SaveScreenshot(genname (options["output"], n, [ll_string, "white", "4k"]), renderView1, ImageResolution=[3840, 2160])  # TransparentBackground=1
        # renderView1.Background = [0., 0., 0.]
        # SaveScreenshot(genname (options["output"], n, [ll_string, "black", "HD"]), renderView1, ImageResolution=[1920, 1080])
        # SaveScreenshot(genname (options["output"], n, [ll_string, "black", "4k"]), renderView1, ImageResolution=[3840, 2160])  # TransparentBackground=1
        # #
        # SaveScreenshot(genname (options["output"], n, [ll_string, "transparent", "HD"]), renderView1, ImageResolution=[1920, 1080], TransparentBackground=1)
        ll_string=""
        lonlat = [-30, 0]
        view.CameraPosition = sphere_proj(lon=lonlat[0], lat=lonlat[1])
        ll_string="%.0fN_%.0fE"%(lonlat[1],lonlat[0])
        #         view.CameraParallelScale = 120

        SaveScreenshot(genname (options["output"], n, [ll_string, "transparent", "4k"]), renderView1, ImageResolution=[3840, 2160], TransparentBackground=1)
        view.CameraPosition = oldpos
        ll_string=""            


        if options.get("lonlat", False ):
            lonlat = options["lonlat"]
            view.CameraPosition = sphere_proj(lon=lonlat[0], lat=lonlat[1])
            ll_string="%.0fN_%.0fE"%(lonlat[1],lonlat[0])
            #            view.CameraParallelScale = 120
            SaveScreenshot(genname (options["output"], n, [ll_string, "transparent", "4k"]), renderView1, ImageResolution=[3840, 2160], TransparentBackground=1)
            view.CameraPosition = oldpos
            ll_string=""            



#! /work/kv0653/spack-flo/paraview-5.10.1-nwsds7/bin/pvbatch
#### import the simple module from the paraview
from paraview.simple import *
import sys
from math import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

print (sys.argv)


from argparse import ArgumentParser
import glob

r = 126400.
def sphere_proj (lat, lon) :
    global r
    theta = (90 - lat) * pi / 180.
    phi = lon * pi / 180.
    z = r * cos (theta)
    x = r * sin(theta) * cos (phi)
    y = r * sin(theta) * sin (phi)
    return (x, y, z)

options = {}
def parse_args():
    '''Parses the command line arguments'''
    global options
    parser = ArgumentParser()
    parser.description = "Plot icon files"
    parser.add_argument("--clouds")
    parser.add_argument("--cloud_expr")
    parser.add_argument("--cloud_vars")
    parser.add_argument("--ocean")
    parser.add_argument("--sea_ice")
    parser.add_argument("--snow")
    parser.add_argument("--psl",)
    parser.add_argument("--psl_var",)
    parser.add_argument("--light")
    parser.add_argument("--lonlat", nargs = 2, type=float)
    parser.add_argument("--output", "-o")
    parser.add_argument("--no_time_string", action="store_true")
    op = parser.parse_args()
    options = vars(op)
    print (options)
    return options




options = {'clouds': None, 'cloud_expr': None, 'cloud_vars': None, 'ocean': '/scratch/k/k202134/tmp/ngc2012_oce_0-200m_3h_inst_20210201T000000Z.nc', 'sea_ice': '/scratch/k/k202134/tmp/ngc2012_oce_2d_1h_inst_20210201T000000Z.nc', 'snow': '/scratch/k/k202134/tmp/ngc2012_lnd_2d_3h_inst_20210201T000000Z.nc', 'psl': None, 'psl_var': None, 'light': None, 'lonlat': None, 'output': 'sic.png', 'no_time_string': False}
# options=parse_args()




# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

# get the material library
materialLibrary1 = GetMaterialLibrary()

# Create a new 'Render View'
renderView1 = CreateView('RenderView')
renderView1.ViewSize = [3840, 2160]
renderView1.AxesGrid = 'GridAxes3DActor'
renderView1.OrientationAxesVisibility = 0
renderView1.CameraPosition = [1144.0024360966738, -682.5869061407763, 142.4868048724324]
renderView1.CameraViewUp = [0, 0., 1]
renderView1.CameraViewAngle = 22. * 1264. / r
renderView1.CameraFocalDisk = 1.0
renderView1.CameraParallelScale = 564.2333216713644
renderView1.EyeAngle = 1.0
renderView1.Background = [1.0, 1.0, 1.0]
renderView1.BackEnd = 'OSPRay raycaster'
renderView1.EnvironmentNorth = [0.0, 0.0, 0.0]
renderView1.OSPRayMaterialLibrary = materialLibrary1

def fancy_light():
    renderView1 = GetActiveViewOrCreate('RenderView')
    renderView1.KeyLightIntensity = 0.0
    # Create a new 'Light'
    light1 = CreateLight()
    light1.Intensity = 1.0
    light1.Position = [50.0, -87.0, -28.6]

    # Create a new 'Light'
    light2 = CreateLight()
    light2.Coords = 'Ambient'
    light2.Intensity = 0.3
    light2.Position = [0.0, -100.0, 0.0]
    light2.DiffuseColor = [0.7529411764705882, 0.8196078431372549, 1.0]

    # Create a new 'Light'
    light3 = CreateLight()
    light3.Enable = 0
    light3.Position = [562.3115794309297, -528.4831081310286, 194.20829319495272]
    light3.FocalPoint = [562.3058234478694, -528.4776977247036, 194.2062917442533]
    renderView1.AdditionalLights = [light1, light2, light3]


SetActiveView(None)

# ----------------------------------------------------------------
# setup view layouts
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# restore active view
SetActiveView(renderView1)
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------


sys.path.append ("/home/k/k202134/Paraview/DYAMOND_WINTER/")
sys.path.append ("/Users/flo/Paraview/DYAMOND_WINTER/")
import velmag
print ("Plotting ocean from " , options["ocean"].split(",") )
(oce, velmag) = velmag.add_ocean(options["ocean"].split(","), interpolate_time = False, level=0)

import seaice
print ("Plotting sea ice")
if not options.get("sea_ice"):
    ice = seaice.plot_sea_ice(oce, use_transform = True, scale=1.001)
else:
    ice = seaice.plot_sea_ice(files=options["sea_ice"].split(","), use_transform = False, scale=1.001)

import plot_snow
print ("Plotting snow from " , options["snow"].split(",") ,)
(snow_file, snow_display) = plot_snow.plot_snow(options["snow"].split(","),  scale=1.00)

# create a new 'NetCDF Time Annotation'
netCDFTimeAnnotation1 = NetCDFTimeAnnotation(registrationName='NetCDFTimeAnnotation1', Input=oce)
netCDFTimeAnnotation1.Expression = '"%02i-%02i-%02i %02i:%02i" % (Date[0], Date[1], Date[2], Date[3], Date[4])'

if not options.get("no_time_string", False):
    # show data in view
    netCDFTimeAnnotation1Display = Show(netCDFTimeAnnotation1, renderView1, 'TextSourceRepresentation')
    netCDFTimeAnnotation1Display.FontSize = 96
    netCDFTimeAnnotation1Display.FontFamily = 'Courier'

import time_dependent_light
time_dependent_light.adjust_light_to_calendar(oce)


    
earth = Sphere(registrationName='Sphere1')
earth.Radius = 199.
earth.ThetaResolution = 3200
earth.PhiResolution = 1600


print ("Setting up textures", file=sys.stderr)
# create a new 'Texture Map to Sphere'
print (earth)
night = TextureMaptoSphere(registrationName='night', Input=earth)
night.PreventSeam = 0

# create a new 'Texture Map to Sphere'
day = TextureMaptoSphere(registrationName='day', Input=earth)
day.PreventSeam = 0

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# show data from day
dayDisplay = Show(day, renderView1, 'UnstructuredGridRepresentation')
# a texture
world2 = CreateTexture('/home/k/k202134/world.200408.3x5400x2700_for_pv.jpg')


# trace defaults for the display properties.
dayDisplay.Representation = 'Surface'
dayDisplay.ColorArrayName = [None, '']
dayDisplay.SelectTCoordArray = 'Texture Coordinates'
dayDisplay.SelectNormalArray = 'None'
dayDisplay.SelectTangentArray = 'None'
dayDisplay.Texture = world2
dayDisplay.OSPRayScaleArray = 'Texture Coordinates'
dayDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
dayDisplay.SelectOrientationVectors = 'None'
dayDisplay.ScaleFactor = 40.0
dayDisplay.SelectScaleArray = 'None'
dayDisplay.GlyphType = 'Arrow'
dayDisplay.GlyphTableIndexArray = 'None'
dayDisplay.GaussianRadius = 2.0
dayDisplay.SetScaleArray = ['POINTS', 'Texture Coordinates']
dayDisplay.ScaleTransferFunction = 'PiecewiseFunction'
dayDisplay.OpacityArray = ['POINTS', 'Texture Coordinates']
dayDisplay.OpacityTransferFunction = 'PiecewiseFunction'
dayDisplay.DataAxesGrid = 'GridAxesRepresentation'
dayDisplay.PolarAxes = 'PolarAxesRepresentation'


# show data from night
# nightDisplay = Show(night, renderView1, 'UnstructuredGridRepresentation')


# a texture
blackMarble_2016_3km_for_pv = CreateTexture('/home/k/k202134/BlackMarble_2016_3km_for_pv-dark.png')

# # trace defaults for the display properties.
# nightDisplay.Representation = 'Surface'
# nightDisplay.ColorArrayName = [None, '']
# nightDisplay.Specular = 0.99
# nightDisplay.SpecularPower = 10.0
# nightDisplay.Ambient = 0.33
# nightDisplay.SelectTCoordArray = 'Texture Coordinates'
# nightDisplay.SelectNormalArray = 'None'
# nightDisplay.SelectTangentArray = 'None'
# nightDisplay.Texture = blackMarble_2016_3km_for_pv
# nightDisplay.OSPRayScaleArray = 'Texture Coordinates'
# nightDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
# nightDisplay.SelectOrientationVectors = 'None'
# nightDisplay.ScaleFactor = 40.0
# nightDisplay.SelectScaleArray = 'alpha'
# nightDisplay.GlyphType = 'Arrow'
# nightDisplay.GlyphTableIndexArray = 'alpha'
# nightDisplay.GaussianRadius = 2.0
# nightDisplay.SetScaleArray = ['POINTS', 'Texture Coordinates']
# nightDisplay.ScaleTransferFunction = 'PiecewiseFunction'
# nightDisplay.OpacityArray = ['POINTS', 'Texture Coordinates']
# nightDisplay.OpacityTransferFunction = 'PiecewiseFunction'
# nightDisplay.DataAxesGrid = 'GridAxesRepresentation'
# nightDisplay.PolarAxes = 'PolarAxesRepresentation'
# nightDisplay.ScalarOpacityFunction = rsdsPWF
# nightDisplay.ScalarOpacityUnitDistance = 10.049316464272104
# nightDisplay.OpacityArrayName = ['CELLS', 'alpha']

# ----------------------------------------------------------------
# restore active source
SetActiveSource(snow_file)
# ----------------------------------------------------------------


if __name__ == '__main__':
    view = GetActiveView()

    oldpos = view.CameraPosition
    
    print ('in main', file=sys.stderr)
    reader=GetActiveSource()
    tsteps=reader.TimestepValues
    print ("Timesteps: ", ", ".join((str (x) for x in tsteps)))
    def genname(fn, n, atts):
        attstr = "_".join((x for x in atts if x))
        return "{fn}_{atts}_{n:03d}.png".format(fn=fn, n=n, atts=attstr)
    for n,t in enumerate (tsteps):
        print ("rendering for time %f"%t)
        if len (tsteps) > 1:
            view.ViewTime = t

        ll_string=""
        lonlat = [-30, 0]
        view.CameraPosition = sphere_proj(lon=lonlat[0], lat=lonlat[1])
        ll_string="%.0fN_%.0fE"%(lonlat[1],lonlat[0])
        #         view.CameraParallelScale = 120

        SaveScreenshot(genname (options["output"], n, [ll_string, "transparent", "4k"]), renderView1, ImageResolution=[3840, 2160], TransparentBackground=1)
        view.CameraPosition = oldpos
        ll_string=""            


        if options.get("lonlat", False ):
            lonlat = options["lonlat"]
            view.CameraPosition = sphere_proj(lon=lonlat[0], lat=lonlat[1])
            ll_string="%.0fN_%.0fE"%(lonlat[1],lonlat[0])
            SaveScreenshot(genname (options["output"], n, [ll_string, "transparent", "4k"]), renderView1, ImageResolution=[3840, 2160], TransparentBackground=1)
            view.CameraPosition = oldpos
            ll_string=""            



#!/bin/bash

# if [ ! -f $s/icon_data.nc ] ; then 
    cdo -O  -setgrid,/pool/data/ICON/grids/public/mpim/0015/icon_grid_0015_R02B09_G.nc -merge -expr,'cloud=clivi*5+cllvi;rsdt=rsdt' -select,date=2020-01-22T16:00:00,name=clivi,cllvi,rsdt    /work/ka1081/NextGEMS/MPIM-DWD-DKRZ/ICON-SAP-5km/Cycle1/atm/30min/clivi/dpp0052/2d/gn/clivi_30min_ICON-SAP-5km_Cycle1_dpp0052_2d_gn_20200122000000-20200122235920.nc  $s/icon-cloud.nc
    cdo -O -setgrid,/pool/data/ICON/grids/public/mpim/0015/icon_grid_0015_R02B09_G.nc    -select,date=2020-01-22T16:00:00,name=psl /work/ka1081/NextGEMS/MPIM-DWD-DKRZ/ICON-SAP-5km/Cycle1/atm/30min/psl/dpp0052/2d/gn/psl_30min_ICON-SAP-5km_Cycle1_dpp0052_2d_gn_20200122000000-20200122235920.nc $s/icon_psl.nc
    cdo -O -merge $s/icon-cloud.nc $s/icon_psl.nc $s/icon_data.nc
    ncatted -a level_type,cloud,o,c,atm -a level_type,psl,o,c,atm  -a level_type,rsdt,o,c,atm -a units,cloud,o,c,one -a long_name,cloud,o,c,cloudiness $s/icon_data.nc
# fi






cdo  -setgrid,/work/ka1081/NextGEMS/ECMWF-AWI/IFS-NEMO-DEEPon-4km/Cycle1/atm/fx/gn/grid.nc   -select,date=2020-01-22T16:00:00  /work/ka1081/NextGEMS/ECMWF-AWI/IFS-NEMO-DEEPon-4km/Cycle1/atm/1hr/clivi/r1i1p1f1/2d/gn/clivi_1hr_IFS-NEMO-DEEPon-4km_Cycle1_r1i1p1f1_2d_gn_20200122000000-20200122230000.nc  $s/ifs-DEEPon_clivi.nc 
cdo  -setgrid,/work/ka1081/NextGEMS/ECMWF-AWI/IFS-NEMO-DEEPon-4km/Cycle1/atm/fx/gn/grid.nc   -select,date=2020-01-22T16:00:00  /work/ka1081/NextGEMS/ECMWF-AWI/IFS-NEMO-DEEPon-4km/Cycle1/atm/1hr/clwvi/r1i1p1f1/2d/gn/clwvi_1hr_IFS-NEMO-DEEPon-4km_Cycle1_r1i1p1f1_2d_gn_20200122000000-20200122230000.nc  $s/ifs-DEEPon_clwvi.nc 
cdo -merge $s/ifs-DEEPon_clivi.nc $s/ifs-DEEPon_clwvi.nc $s/ifs-DEEPon_raw.nc 
cdo -expr,cloud=clivi*5+clwvi $s/ifs-DEEPon_raw.nc $s/ifs-DEEPon-cloud.nc


cdo  -setgrid,/work/ka1081/NextGEMS/ECMWF-AWI/IFS-NEMO-4km/Cycle1/atm/fx/gn/grid.nc   -select,date=2020-01-22T16:00:00  /work/ka1081/NextGEMS/ECMWF-AWI/IFS-NEMO-4km/Cycle1/atm/1hr/clivi/r1i1p1f1/2d/gn/clivi_1hr_IFS-NEMO-4km_Cycle1_r1i1p1f1_2d_gn_20200122000000-20200122230000.nc  $s/ifs_clivi.nc 
cdo  -setgrid,/work/ka1081/NextGEMS/ECMWF-AWI/IFS-NEMO-4km/Cycle1/atm/fx/gn/grid.nc   -select,date=2020-01-22T16:00:00  /work/ka1081/NextGEMS/ECMWF-AWI/IFS-NEMO-4km/Cycle1/atm/1hr/clwvi/r1i1p1f1/2d/gn/clwvi_1hr_IFS-NEMO-4km_Cycle1_r1i1p1f1_2d_gn_20200122000000-20200122230000.nc  $s/ifs_clwvi.nc 
cdo -merge $s/ifs_clivi.nc $s/ifs_clwvi.nc $s/ifs_raw.nc 
cdo -expr,cloud=clivi*5+clwvi $s/ifs_raw.nc $s/ifs-cloud.nc


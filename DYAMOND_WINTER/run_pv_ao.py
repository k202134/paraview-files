#!/usr/bin/env python

from argparse import ArgumentParser
import glob

options = {}
def parse_args():
    '''Parses the command line arguments'''
    global options
    parser = ArgumentParser()
    parser.description = "Plot icon files"
    parser.add_argument("--oce", "-O")
    parser.add_argument("--atm", "-A")
    parser.add_argument("--ice", "-I")
    op = parser.parse_args()
    options = vars(op)
    if options.get("oce", False):
        print ("Globbing for " + options["oce"])
        options["oce"]=glob.glob(options["oce"])
    if options.get("atm", False):
        print ("Globbing for " + options["atm"])
        options["atm"]=glob.glob(options["atm"])
    if options.get("ice", False):
        options["ice"]=glob.glob(options["ice"])
    return options

# state file generated using paraview version 5.8.1-2374-gdd080cd

#### import the simple module from the paraview
from paraview.simple import *
import sys


def cerr(*objs):
  print( *objs, file=sys.stderr)

def add_ocean(files, interpolate_time=True, level=None):
    cerr ("velmag.add_ocean starting for ", files)
    renderView1 = GetActiveViewOrCreate('RenderView')
    # create a new 'CDIReader'
    cerr ("Opening file")
    ocean = CDIReader(registrationName='ocean', FileNames=files)
    ocean.Dimensions = '(lon, lat, depth)'
    ocean.CellArrayStatus = ['u', 'v']
    ocean.SetProjection = 'Spherical Projection'
    ocean.LayerThickness = 50
    ocean.MaskingValueVar = 'u'
    if level is not None:
      ocean.VerticalLevel = level

    source = ocean
    if interpolate_time:
      cerr ("Adding time interpolation to 15 min")
      oceanTI=TemporalInterpolator(Input=ocean, ResampleFactor=4)
      source = oceanTI
      
    cerr ("Adding a calculator for the magnitude")
    # create a new 'Calculator'
    velmag = Calculator(registrationName='Velmag', Input=source)
    velmag.AttributeType = 'Cell Data'
    velmag.ResultArrayName = 'vel'
    velmag.Function = 'u*iHat + v *jHat'

    cerr ("Showing")
    # show data from velmag
    velmagDisplay = Show(velmag, renderView1, 'UnstructuredGridRepresentation')

    cerr ("Configuring")
    # get color transfer function/color map for 'vel'
    velLUT = GetColorTransferFunction('vel')
    velLUT.AutomaticRescaleRangeMode = 'Never'
    velLUT.RGBPoints = [0.0, 0.031373, 0.25098, 0.505882, 0.06274500000000005, 0.031373, 0.329719, 0.590527, 0.12549, 0.031911, 0.408397, 0.674787, 0.1882355, 0.100807, 0.479262, 0.710219, 0.25098050000000005, 0.169704, 0.550219, 0.745744, 0.3137255, 0.238601, 0.62699, 0.787082, 0.37647049999999993, 0.307958, 0.703114, 0.826759, 0.4392155, 0.39654, 0.752326, 0.797232, 0.501960785, 0.485121, 0.801046, 0.767705, 0.564706, 0.573702, 0.83451, 0.738178, 0.627451, 0.661592, 0.867743, 0.711034, 0.690196, 0.732457, 0.895302, 0.74253, 0.7529410000000001, 0.801845, 0.922307, 0.774579, 0.8156865, 0.841215, 0.938055, 0.817885, 0.8784315, 0.880907, 0.95391, 0.861084, 0.9411765000000001, 0.926182, 0.971626, 0.902422, 1.0, 0.968627, 0.988235, 0.941176]
    velLUT.ColorSpace = 'Lab'
    velLUT.NanColor = [1.0, 0.0, 0.0]
    velLUT.ScalarRangeInitialized = 1.0

    # get opacity transfer function/opacity map for 'vel'
    velPWF = GetOpacityTransferFunction('vel')
    velPWF.ScalarRangeInitialized = 1

    # trace defaults for the display properties.
    velmagDisplay.Representation = 'Surface'
    velmagDisplay.ColorArrayName = ['CELLS', 'vel']
    velmagDisplay.LookupTable = velLUT
    velmagDisplay.SelectTCoordArray = 'None'
    velmagDisplay.SelectNormalArray = 'None'
    velmagDisplay.SelectTangentArray = 'None'
    velmagDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
    velmagDisplay.SelectOrientationVectors = 'None'
    velmagDisplay.ScaleFactor = 59.997473144531256
    velmagDisplay.SelectScaleArray = 'None'
    velmagDisplay.GlyphType = 'Arrow'
    velmagDisplay.GlyphTableIndexArray = 'None'
    velmagDisplay.GaussianRadius = 2.9998736572265625
    velmagDisplay.SetScaleArray = [None, '']
    velmagDisplay.ScaleTransferFunction = 'PiecewiseFunction'
    velmagDisplay.OpacityArray = [None, '']
    velmagDisplay.OpacityTransferFunction = 'PiecewiseFunction'
    velmagDisplay.DataAxesGrid = 'GridAxesRepresentation'
    velmagDisplay.PolarAxes = 'PolarAxesRepresentation'
    velmagDisplay.ScalarOpacityFunction = velPWF
    velmagDisplay.ScalarOpacityUnitDistance = 10.74003524973295
    velmagDisplay.OpacityArrayName = ['CELLS', 'u']

    velmagDisplay.Scale = [1.0005, 1.0005, 1.0005]
    velmagDisplay.DataAxesGrid.Scale = [1.0005, 1.0005,1.0005]
    velmagDisplay.PolarAxes.Scale = [1.0005, 1.0005, 1.0005]
    
    velmagDisplay.Ambient = 0.2
    velmagDisplay.SpecularPower = 70.0
    velmagDisplay.Diffuse = 0.5
    
    velLUTColorBar = GetScalarBar(velLUT, renderView1)
    velLUTColorBar.Title = 'vel'
    velLUTColorBar.ComponentTitle = 'Magnitude'

    # set color bar visibility
    velLUTColorBar.Visibility = 0


    velmagDisplay.SetScalarBarVisibility(renderView1, False)
    cerr ("Done for velmag.add_ocean")
    print (source)
    return (source, velmag)
